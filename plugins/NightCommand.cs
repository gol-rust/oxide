using System;
using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Libraries.Covalence;

namespace Oxide.Plugins
{
    [Info("NightCommand", "The Noxes", "0.0.1")]
    [Description("Run commands at night")]
    public class NightCommand : CovalencePlugin
    {
        public bool NightCommandSent = false;
        public bool DayCommandSent = false;
        public float DawnTime = 6f;
        public float DuskTime = 21f;

        private void OnServerInitialized()
        {
            timer.Repeat(1, 0, CheckTime);
        }

        private void Broadcast(string message)
        {
            foreach (var player in players.Connected) player.Reply(message);
        }
        
        bool IsNight => server.Time.TimeOfDay.Hours < DawnTime || server.Time.TimeOfDay.Hours >= DuskTime;

        private void CheckTime()
        {            
            if (IsNight && !NightCommandSent)
            {
                Broadcast("Night is upon us, beware!");
                //server.Command("oxide.load BotSpawnNight");
                NightCommandSent = true;
                DayCommandSent = false;
            }
            else
            if (!IsNight && !DayCommandSent)
            {
                //server.Command("oxide.unload BotSpawnNight");
                DayCommandSent = true;
                NightCommandSent = false;
            }
        }
    }
}