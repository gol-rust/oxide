﻿using Oxide.Core.Configuration;
using Oxide.Core.Plugins;
using Oxide.Core.Libraries.Covalence;
using Oxide.Game.Rust.Cui;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("GimmeCookie", "Cookiemonster", "6.6.6")]
    [Description("C IS FOR COOKIE")]
    

    public class GimmeCookie : RustPlugin
    {
        public static ulong ToolSkinID = 1325242862;
        public static string ToolItemShortname = "jar.pickle";
        public static string ToolName = "Cookie";
        private const string usePerm = "gimmecookie.use";

        void Init()
        {
            permission.RegisterPermission(usePerm, this);
        }

        
        [ChatCommand("cookie")]
        void GiveCookie_command(BasePlayer player, string command, string[] args)
        {
            if (!permission.UserHasPermission(player.UserIDString, usePerm))
            {
                SendReply(player, "No cookies for you.");
                return;
            }
            int number = 1;
            if (args.Length > 0)
            {
                number = Convert.ToInt32(args[0]);
            }
        
            Item item = ItemManager.CreateByPartialName(ToolItemShortname, number);
            if (item == null)
            {
                SendReply(player, "WARNING! Failed to summon cookie.");
                return;
            }
            item.skin = ToolSkinID;
            item.name = ToolName;

            player.GiveItem(item);
        }
    }
}
