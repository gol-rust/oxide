﻿using System;
using System.Collections.Generic;
using System.Linq;
using Oxide.Core;
using Newtonsoft.Json;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("Ent Spawn Point", "The Noxes", "1.3.2")]
    [Description("Create a spawn point for entities")]
    public class EntSpawnPoint : RustPlugin
    {
        #region config
        Configuration config;
        public class Configuration
        {
            [JsonProperty(PropertyName = "Clear spawnpoints on wipe")]
            public bool wipeData { get; set; } = false;
            [JsonProperty(PropertyName = "Hour day time starts")]
            public float dayTime { get; set;} = 6f;
            [JsonProperty(PropertyName = "Hour night time starts")]
            public float nightTime { get; set; } = 21f;
        }

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                config = Config.ReadObject<Configuration>();
                if (config == null)
                    LoadDefaultConfig();
            }
            catch
            {
                LoadDefaultConfig();
            }
            SaveConfig();
        }

        protected override void LoadDefaultConfig()
        {
            Puts("Generating new configuration file");
            config = new Configuration();
        }

        protected override void SaveConfig() => Config.WriteObject(config);
        #endregion

        #region data
        class StoredData
        {
            public Dictionary<int, PointData> spawnpoints = new Dictionary<int, PointData>();
            public StoredData() { }
        }

        class PointData
        {
            public List<float> location = new List<float>();
            public string name;
            public string entity;
            public float respawn;
            public int amount;
            public string timeofday;
            public int radius;
            public PointData() { }
            public PointData(Vector3 targetpos, string name, string entity, float respawn, int amount, string timeofday, int radius)
            {
                location = new List<float> { targetpos.x, targetpos.y, targetpos.z };
                this.name = name;
                this.entity = entity;
                this.respawn = respawn;
                this.amount = amount;
                this.timeofday = timeofday;
                this.radius = radius;
            }
        }

        StoredData storedData;
        PointData pointData;

        void SaveData() => Interface.Oxide.DataFileSystem.WriteObject("EntSpawnPoint", storedData);
        #endregion

        const string permAllowed = "entspawnpoint.use";
        System.Random rnd = new System.Random();
        Dictionary<int, List<BaseEntity>> spawnedEntities = new Dictionary<int, List<BaseEntity>>();
        Dictionary<int, Timer> activeTimers = new Dictionary<int, Timer>();
        Dictionary<string, string> availableEntities = new Dictionary<string, string>
        {
            ["scientist"] = "assets/prefabs/npc/scientist/scientist.prefab",
            ["scarecrow"] = "assets/prefabs/npc/scarecrow/scarecrow.prefab",
            ["murderer"] = "assets/prefabs/npc/murderer/murderer.prefab",
            //["chinook"] = "assets/prefabs/npc/ch47/ch47scientists.entity.prefab",
            //["helicopter"] = "assets/prefabs/npc/patrol helicopter/patrolhelicopter.prefab",
            //["tank"] = "assets/prefabs/npc/m2bradley/bradleyapc.prefab",
            ["chicken"] = "assets/rust.ai/agents/chicken/chicken.prefab",
            ["bear"] = "assets/rust.ai/agents/bear/bear.prefab",
            ["boar"] = "assets/rust.ai/agents/boar/boar.prefab",
            ["horse"] = "assets/rust.ai/agents/horse/horse.prefab",
            ["stag"] = "assets/rust.ai/agents/stag/stag.prefab",
            ["wolf"] = "assets/rust.ai/agents/wolf/wolf.prefab"
        };

        void OnNewSave(string filename)
        {
            if (config.wipeData)
            {
                storedData = new StoredData();
                SaveData();
            }
        }
        void OnServerInitialized()
        {
            storedData = Interface.Oxide.DataFileSystem.ReadObject<StoredData>("EntSpawnPoint");
            permission.RegisterPermission(permAllowed, this);
            foreach (KeyValuePair<int, PointData> entry in storedData.spawnpoints)
                CreateTimer(entry.Key, entry.Value.entity, entry.Value.respawn, entry.Value.amount, entry.Value.timeofday);
        }

        void Unload()
        {
            foreach (List<BaseEntity> entList in spawnedEntities.Values)
            {
                foreach (BaseEntity ent in entList)
                    ent.Kill();
            }
        }

        void OnEntityDeath(BaseCombatEntity entity, HitInfo info)
        {
            BaseEntity baseEnt = entity as BaseEntity;
            foreach (KeyValuePair<int, List<BaseEntity>> entry in spawnedEntities)
            {
                int uid = entry.Key;
                List<BaseEntity> entList = entry.Value;
                if (entList.Contains(entity))
                    spawnedEntities[uid].Remove(entity);
            }
        }

        void CreateTimer(int uid, string entity, float respawn, int amount, string timeofday)
        {
            if (timeofday == "all")
            {
                for (int i = 0; i < amount; i++)
                    SpawnEntity(GetRandomLocation(uid), entity, uid);
                Timer respawnTimer = timer.Every(respawn * 60, () =>
                {
                    for (int i = 0; i < amount - spawnedEntities[uid].Count(); i++)
                        SpawnEntity(GetRandomLocation(uid), entity, uid);
                });
                activeTimers[uid] = respawnTimer;
            }
            if (timeofday == "night")
            {
                if (IsNight)
                {
                    for (int i = 0; i < amount; i++)
                        SpawnEntity(GetRandomLocation(uid), entity, uid);
                }
                Timer respawnTimer = timer.Every(respawn * 60, () =>
                {
                    if (IsNight)
                    {
                        int spawnCount = 0;
                        if (spawnedEntities.ContainsKey(uid))
                            spawnCount = spawnedEntities[uid].Count();
                        for (int i = 0; i < amount - spawnCount; i++)
                            SpawnEntity(GetRandomLocation(uid), entity, uid);
                    }
                    if (!IsNight)
                    {
                        if (spawnedEntities.ContainsKey(uid))
                        {
                            foreach (BaseEntity ent in spawnedEntities[uid])
                            {
                                if (!ent.IsDestroyed)
                                    ent.Kill();
                            }
                            spawnedEntities.Remove(uid);
                        }
                    }
                });
                activeTimers[uid] = respawnTimer;
            }
            if (timeofday == "day")
            {
                if (!IsNight)
                {
                    for (int i = 0; i < amount; i++)
                        SpawnEntity(GetRandomLocation(uid), entity, uid);
                }
                Timer respawnTimer = timer.Every(respawn * 60, () =>
                {
                    if (!IsNight)
                    {
                        int spawnCount = 0;
                        if (spawnedEntities.ContainsKey(uid))
                            spawnCount = spawnedEntities[uid].Count();
                        for (int i = 0; i < amount - spawnCount; i++)
                            SpawnEntity(GetRandomLocation(uid), entity, uid);
                    }
                    if (IsNight)
                        if (spawnedEntities.ContainsKey(uid))
                        {
                            foreach (BaseEntity ent in spawnedEntities[uid])
                            {
                                if (!ent.IsDestroyed)
                                    ent.Kill();
                            }
                        }
                });
                activeTimers[uid] = respawnTimer;
            }
        }

        void SendConsoleReply(Network.Connection cn, string msg)
        {
            if (Network.Net.sv.IsConnected())
            {
                Network.Net.sv.write.Start();
                Network.Net.sv.write.PacketID(Network.Message.Type.ConsoleMessage);
                Network.Net.sv.write.String(msg);
                Network.Net.sv.write.Send(new Network.SendInfo(cn));
            }
        }

        int GetId()
        {
            int LastID = 0;
            if (storedData.spawnpoints.Count() > 0)
                LastID = storedData.spawnpoints.Keys.Last();
            int uid = LastID + 1;
            return uid;
        }

        bool IsNight => ConVar.Env.time < config.dayTime || ConVar.Env.time >= config.nightTime;

        Vector3 GetRandomLocation(int uid)
        {
            storedData.spawnpoints.TryGetValue(uid, out pointData);
            Vector3 spawnlocation = new Vector3(pointData.location[0], pointData.location[1], pointData.location[2]);
            int radius = pointData.radius;
            Vector3 newLocation = new Vector3((spawnlocation.x + rnd.Next(-radius, radius + 1)), (spawnlocation.y + 5), (spawnlocation.z + rnd.Next(-radius, radius + 1)));
            Vector3 downwards = new Vector3(0, -1, 0);
            var ray = new Ray(newLocation, downwards);
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 5000);
            Vector3 location = hit.point;
            return location;
        }

        void SpawnEntity(Vector3 location, string entity, int uid)
        {
            BaseEntity ent = GameManager.server.CreateEntity(availableEntities[entity], location);
            ent.Spawn();
            List<BaseEntity> entList = new List<BaseEntity>();
            if (spawnedEntities.ContainsKey(uid))
                entList = spawnedEntities[uid];
            entList.Add(ent);
            spawnedEntities[uid] = entList;
        }

        void SendHelpText(BasePlayer player)
        {
            SendReply(player, "<color=#ffa500ff>/sp add {name} {entity} {amount} {time of day} {respawn in minutes} {radius}</color>\nto add a spawnpoint");
            SendReply(player, "<color=#ffa500ff>/sp remove {id}</color>\nto remove a spawnpoint");
            SendReply(player, "<color=#ffa500ff>/sp list</color>\nto list all current spawnpoints");
            SendReply(player, "<color=#ffa500ff>/sp entities</color>\nto list all available entities");
        }

        [ChatCommand("sp")]
        void spCommand(BasePlayer player, string command, string[] args)
        {
            if (!permission.UserHasPermission(player.UserIDString, permAllowed))
            {
                SendReply(player, "You do not have permission to use that command.");
                return;
            }
            List<string> argsList = new List<string>(args);
            if (argsList.Count == 0)
            {
                SendHelpText(player);
                return;
            }
            switch (argsList[0].ToLower())
            {
                case "add":
                    {
                        if (argsList.Count < 6)
                        {
                            SendReply(player, "<color=#ffa500ff>{name} {entity} {amount} {respawn in minutes} {time of day}</color> must be filled in. <color=#ffa500ff>{radius}</color> is optional (default: 5m)\nValid values for <color=#ffa500ff>{time of day}</color> are \"all\", \"day\" or \"night\"");
                            return;
                        }
                        if (!(args[5] == "all" || args[5] == "night" || args[5] == "day"))
                        {
                            SendReply(player, "Valid values for {time of day} are \"all\", \"day\" or \"night\"");
                            return;
                        }
                        Vector3 location = player.transform.position;
                        int uid = GetId();
                        string name = args[1];
                        string entity = args[2];
                        int amount = Convert.ToInt32(args[3]);
                        float respawn = Convert.ToInt32(args[4]);
                        string timeofday = args[5];
                        int radius = 5;
                        if (argsList.Count > 6)
                        {
                            radius = Convert.ToInt32(args[6]);
                        }
                        pointData = new PointData(location, name, entity, respawn, amount, timeofday, radius);
                        storedData.spawnpoints.Add(uid, pointData);
                        SaveData();
                        string timetext = "all day long";
                        if (timeofday == "night")
                            timetext = "at night";
                        if (timeofday == "day")
                            timetext = "during the day";
                        SendReply(player, String.Format("#{0} - New spawnpoint <color=#ffa500ff>{1}</color> added at <color=#ffa500ff>{2}</color>, with a <color=#ffa500ff>{3}m</color> radius for <color=#ffa500ff>{4} {5}(s)</color> on a <color=#ffa500ff>{6}</color> minute respawn timer, <color=#ffa500ff>{7}</color>", uid, name, location, radius, amount, entity, respawn, timetext));
                        CreateTimer(uid, entity, respawn, amount, timeofday);
                        return;
                    }
                case "remove":
                    {
                        if (argsList.Count < 2)
                        {
                            SendReply(player, "Please provide one or more of the following IDs:");
                            foreach (KeyValuePair<int, PointData> entry in storedData.spawnpoints)
                            {
                                string timetext = "all day long";
                                if (entry.Value.timeofday == "night")
                                    timetext = "at night";
                                if (entry.Value.timeofday == "day")
                                    timetext = "during the day";
                                SendReply(player, String.Format("#{0} {1} - {2} {3}(s), {4}", entry.Key, entry.Value.name, entry.Value.amount, entry.Value.entity, timetext));
                            }
                            return;
                        }
                        argsList.RemoveAt(0);
                        foreach (string sid in argsList)
                        {
                            int uid = Convert.ToInt32(sid);
                            if (spawnedEntities.ContainsKey(uid))
                            {
                                foreach (BaseEntity ent in spawnedEntities[uid])
                                    ent.Kill();
                                spawnedEntities.Remove(uid);
                            }
                            storedData.spawnpoints.Remove(uid);
                            activeTimers[uid].Destroy();
                            activeTimers.Remove(uid);
                            SendReply(player, String.Format("Successfully removed spawnpoint #{0}!", uid));
                        }
                        return;
                    }
                case "list":
                    {
                        SendConsoleReply(player.net.connection, "[EntSpawnPoint] Current spawnpoints:");
                        foreach (KeyValuePair<int, PointData> entry in storedData.spawnpoints)
                        {
                            string timetext = "all day long";
                            if (entry.Value.timeofday == "night")
                                timetext = "at night";
                            if (entry.Value.timeofday == "day")
                                timetext = "during the day";
                            SendConsoleReply(player.net.connection, String.Format("#{0} {1} - {2} {3}(s) at {4} ({5}m radius), {6}, {7} minute respawn\n", entry.Key, entry.Value.name, entry.Value.amount, entry.Value.entity, String.Join(",", entry.Value.location.ToArray()), entry.Value.radius, timetext, entry.Value.respawn));
                        }
                        SendReply(player, "Check your console for a list of currently active spawnpoints.");
                        return;
                    }
                case "entities":
                    {
                        foreach (KeyValuePair<string, string> entities in availableEntities)
                        {
                            SendConsoleReply(player.net.connection, String.Format("{0} ({1})", entities.Key, entities.Value));
                        }
                        SendReply(player, "Check your console for a list of available entities.");
                        return;
                    }
                default:
                    {
                        SendHelpText(player);
                        return;
                    }
            }
        }
    }
}