﻿using Oxide.Core.Plugins;
using System.Linq;
using System;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("SubcarFix", "The Noxes", "2.0.0")]
    [Description("CARS ARE NOT SUBMARINES")]

    public class SubcarFix : RustPlugin
    {
        void OnServerInitialized()
        {
            timer.Repeat(5, 0, IfCarIsSubmarine);
        }

        void IfCarIsSubmarine()
        {
            foreach(BasePlayer player in BasePlayer.activePlayerList)
            {
                if (player.isMounted && player.IsHeadUnderwater())
                {
                    BaseMountable entity = player.GetMounted();
                    var pentity = entity.GetParentEntity();
                    if (entity is BaseVehicleSeat && pentity is BaseCar)
                    {
                        CarGoBoom(pentity, player);
                        return;
                    }
                }
            }
        }

        void CarGoBoom(BaseEntity subcar, BasePlayer player)
        {
            SendReply(player, "<color=#ce422b>CRITICAL ENGINE FAILURE</color>");
            var carpos = subcar.transform.position;
            //var carrot = subcar.transform.rotation;
            //string fireball = "assets/bundled/prefabs/oilfireballsmall.prefab";
            string explosion = "assets/prefabs/tools/c4/effects/c4_explosion.prefab";
            subcar.Kill(BaseNetworkable.DestroyMode.None);
            Effect.server.Run(explosion, carpos);
            //var oilfire = GameManager.server.CreateEntity(fireball, carpos, carrot);
            //oilfire.Spawn();
        }
    }        
}