﻿using Oxide.Core.Plugins;

namespace Oxide.Plugins
{
    [Info("RestartOtherday", "The Noxes", "1.0.1")]
    [Description("Restart the server every other day")]
    public class RestartOtherday : RustPlugin
    {
        [PluginReference]
        Plugin SmoothRestart;

        bool dayPassed = false;

        void OnServerInitialized()
        {
            timer.Every(1f, () => {
                if (System.DateTime.Now.ToString("HH:mm:ss") == "02:59:01")
                {
                    if (dayPassed)
                        rust.RunServerCommand("sr.restart 30");
                    else
                        dayPassed = true;
                }
                
            });
        }
    }
}
