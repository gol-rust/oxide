﻿using System;
using System.Collections.Generic;
using Oxide.Core;
using Oxide.Core.Plugins;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("TCDestructionWarner", "CommanderStrax", "0.2.0")]
    [Description("Tells off a person who damages a TC when it doesn't belong to them!")]
    class TCDestructionWarner: RustPlugin
    {
        [PluginReference]
        Plugin HooksExtended;

        Dictionary<BuildingPrivlidge, List<string>> HasHitCupboard = new Dictionary<BuildingPrivlidge, List<string>>();

        void OnCupboardDamage(BuildingPrivlidge cupboard, HitInfo info)
        {
            BasePlayer attacker = info.InitiatorPlayer;
            if (attacker != null && !cupboard.IsAuthed(attacker))
            {
                if (!HasHitCupboard.ContainsKey(cupboard))
                    HasHitCupboard[cupboard] = new List<string>();
                if (!HasHitCupboard[cupboard].Contains(attacker.UserIDString))
                {
                    HasHitCupboard[cupboard].Add(attacker.UserIDString);
                    SendReply(attacker, "<color=#ce422b>Warning:</color> Refrain from decaying other players bases as this will lead to warnings or bans. Check <color=#ffa500ff>/rules</color>.");
                    timer.Once(60f, () => {
                        HasHitCupboard[cupboard].Remove(attacker.UserIDString);
                    });
                }
            }
        }

        void OnCupboardDeath(BuildingPrivlidge cupboard, HitInfo info)
        {
            BasePlayer killer = info.InitiatorPlayer;
            if (killer != null && !cupboard.IsAuthed(killer))
            {
                SendReply(killer, "<color=#ce422b>Warning:</color> Remember to replace the TC and store enough materials in it to keep it from decaying for a reasonable amount of time.");
                LogToFile("deaths", String.Format("[{0}] {1} ({2}) destroyed a TC at {3}", System.DateTime.Now.ToShortTimeString(), killer.displayName, killer.UserIDString, cupboard.GetEntity().transform.position), this);
            }
        }
    }
}
