using UnityEngine;
using System;
using Newtonsoft.Json;

namespace Oxide.Plugins
{
    [Info("Memorial", "The Noxes", "2.0.0")]
    [Description("Makes sure the memorial stays put")]
    public class Memorial : RustPlugin
    {
        GraveSetup graveSetup;
        public class GraveSetup
        {
            [JsonProperty(PropertyName = "Desired grave prefab string")]
            public string gravePrefab { get; set; } = "assets/bundled/prefabs/autospawn/collectable/stone/halloween/halloween-metal-collectable.prefab";
            [JsonProperty(PropertyName = "x y z coordinates of grave")]
            public string gravePosString { get; set; } = "0,0,0";
            [JsonProperty(PropertyName = "x y z w rotation of grave")]
            public string graveRotString { get; set; } = "0,0,0,0";
        }

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                graveSetup = Config.ReadObject<GraveSetup>();
                if (graveSetup == null)
                    LoadDefaultConfig();
            }
            catch
            {
                LoadDefaultConfig();
            }
            SaveConfig();
        }

        protected override void LoadDefaultConfig()
        {
            Puts("No or faulty config detected. Generating new configuration file");
            graveSetup = new GraveSetup();
        }

        protected override void SaveConfig() => Config.WriteObject(graveSetup);

        Vector3 gravePos;
        Quaternion graveRot;
        BaseEntity placedGrave = new BaseEntity();

        void OnServerInitialized()
        {
            string[] gravePosArray = graveSetup.gravePosString.Split(',');
            string[] graveRotArray = graveSetup.graveRotString.Split(',');
            gravePos = new Vector3(Convert.ToSingle(gravePosArray[0]), Convert.ToSingle(gravePosArray[1]), Convert.ToSingle(gravePosArray[2]));
            graveRot = new Quaternion(Convert.ToSingle(graveRotArray[0]), Convert.ToSingle(graveRotArray[1]), Convert.ToSingle(graveRotArray[2]), Convert.ToSingle(graveRotArray[3]));
            SpawnGrave();
            timer.Every(600, RefreshGrave);
        }

        void Unload()
        {
            placedGrave.Kill();
        }

        void RefreshGrave()
        {
            if (placedGrave != null)
            {
                placedGrave.Kill();
                SpawnGrave();
            }
            else
                SpawnGrave();
        }
        
        void SpawnGrave()
        {
            BaseEntity newGrave = GameManager.server.CreateEntity(graveSetup.gravePrefab, gravePos, graveRot);
            newGrave.Spawn();
            placedGrave = newGrave;
        }
    }
}