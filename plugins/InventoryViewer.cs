﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("Inventory Viewer", "The Noxes", "1.0.1")]
    [Description("Allows players with permission assigned to view anyone's inventory")]
    public class InventoryViewer : RustPlugin
    {
        readonly string RequiredPermission = "inventoryviewer.allowed";

        readonly Dictionary<BasePlayer, List<BasePlayer>> matches = new Dictionary<BasePlayer, List<BasePlayer>>();

        void CheckInventory(BasePlayer player, BasePlayer target)
        {
            if (!target)
                return;

            if (!target.inventory)
                return;

            Network.Connection pc = player.net.connection;
            SendChatMessage(player, "CheckConsole");
            SendConsoleReply(pc, string.Format("[Inventory Viewer] {0} is wearing:", target.displayName));
            foreach (Item item in target.inventory.containerWear.itemList)
                SendConsoleReply(pc, string.Format("{0}", item.info.shortname));
            SendConsoleReply(pc, "With an inventory containing:");
            foreach (Item item in target.inventory.containerBelt.itemList)
                SendConsoleReply(pc, string.Format("{0} {1}", item.amount, item.info.shortname));
            foreach (Item item in target.inventory.containerMain.itemList)
                SendConsoleReply(pc, string.Format("{0} {1}", item.amount, item.info.shortname));
        }

        void Loaded()
        {
            permission.RegisterPermission(RequiredPermission, this);
        }

        new void LoadDefaultMessages()
        {
            lang.RegisterMessages(new Dictionary<string, string>
            {
                { "CheckConsole", "Check your console to see the list of items." },
                { "InvalidArguments", "Invalid argument(s) supplied! Use '/{0} <name>' or '/{0} list <number>'." },
                { "InvalidSelection", "Invalid number, use the number in front of the player's name. Use '/{0} list' to check the list of players again." },
                { "MultiplePlayersFound", "Multiple players found with that name, please select one of these players by using '/{0} list <number>':" },
                { "NoListAvailable", "You do not have a players list available, use '/{0} <name>' instead." },
                { "NoPlayersFound", "Couldn't find any players matching that name." },
                { "NotAllowed", "You are not allowed to use this command." },
                { "TooManyPlayersFound", "Too many players were found, the list of matches is only showing the first 5. Try to be more specific." }
            }, this);
        }

        [ChatCommand("viewinv")]
        void ViewInventoryCommand(BasePlayer player, string command, string[] args)
        {
            if (!CanUseCommand(player))
            {
                SendChatMessage(player, "NotAllowed");
                return;
            }

            if (args.Length < 1)
            {
                SendChatMessage(player, "InvalidArguments", command);
                return;
            }

            if (args[0] == "list")
            {
                if (args.Length == 1)
                {
                    if (!matches.ContainsKey(player) || matches[player] == null)
                    {
                        SendChatMessage(player, "NoListAvailable", command);
                        return;
                    }

                    ShowMatches(player);

                    return;
                }

                int num;
                if (int.TryParse(args[1], out num))
                {
                    if (!matches.ContainsKey(player) || matches[player] == null)
                    {
                        SendChatMessage(player, "NoListAvailable", command);
                        return;
                    }

                    if (num > matches[player].Count)
                    {
                        SendChatMessage(player, "InvalidSelection", command);
                        ShowMatches(player);
                        return;
                    }

                    CheckInventory(player, matches[player][num - 1]);
                    return;
                }

                SendChatMessage(player, "InvalidArguments", command);
            }
            else
            {
                string name = string.Join(" ", args);
                List<BasePlayer> players = FindPlayersByNameOrId(name);

                switch (players.Count)
                {
                    case 0:
                        SendChatMessage(player, "NoPlayersFound", command);
                        break;

                    case 1:
                        CheckInventory(player, players[0]);
                        break;

                    default:
                        SendChatMessage(player, "MultiplePlayersFound", command);

                        if (!matches.ContainsKey(player))
                            matches.Add(player, players);
                        else
                            matches[player] = players;

                        ShowMatches(player);

                        break;
                }
            }
        }

        /// <summary>
        /// Looks up all players (active and sleeping) by a given (partial) name or steam id.
        /// </summary>
        List<BasePlayer> FindPlayersByNameOrId(string nameOrId)
        {
            List<BasePlayer> matches = new List<BasePlayer>();

            foreach (BasePlayer player in BasePlayer.activePlayerList)
            {
                if (!string.IsNullOrEmpty(player.displayName))
                {
                    if (player.displayName.ToLower().Contains(nameOrId.ToLower()))
                    {
                        matches.Add(player);
                    }
                }

                if (player.UserIDString == nameOrId)
                {
                    matches.Add(player);
                }
            }

            foreach (BasePlayer player in BasePlayer.sleepingPlayerList)
            {
                if (!string.IsNullOrEmpty(player.displayName))
                {
                    if (player.displayName.ToLower().Contains(nameOrId.ToLower()))
                    {
                        matches.Add(player);
                    }
                }

                if (player.UserIDString == nameOrId)
                {
                    matches.Add(player);
                }
            }

            return matches.OrderBy(p => p.displayName).ToList();
        }

        /// <summary>
        /// Shows the cached matches for the player.
        /// </summary>
        void ShowMatches(BasePlayer player)
        {
            for (int i = 0; i < matches[player].Count; i++)
            {
                SendChatMessage(player, $"{i + 1}. {matches[player][i].displayName}");

                if (i == 4 && i < matches[player].Count)
                {
                    SendChatMessage(player, "TooManyPlayersFound");
                    break;
                }
            }
        }

        bool CanUseCommand(BasePlayer player)
        {
            return permission.UserHasPermission(player.UserIDString, RequiredPermission);
        }

        void SendChatMessage(BasePlayer player, string key, params string[] args)
        {
            if (args == null || args.Length == 0)
            {
                Player.Reply(player, lang.GetMessage(key, this, player.UserIDString));
            }
            else
            {
                Player.Reply(player, string.Format(lang.GetMessage(key, this, player.UserIDString), args));
            }
        }

        void SendConsoleReply(Network.Connection cn, string msg)
        {
            if (Network.Net.sv.IsConnected())
            {
                Network.Net.sv.write.Start();
                Network.Net.sv.write.PacketID(Network.Message.Type.ConsoleMessage);
                Network.Net.sv.write.String(msg);
                Network.Net.sv.write.Send(new Network.SendInfo(cn));
            }
        }
    }
}