﻿using Oxide.Core;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("All Barricades Must Die", "The Noxes", "1.0.0")]
    [Description("Get rid of these silly stacking barricades")]

    class AllBarricadesMustDie : RustPlugin
    {
        const string barricadeFamily = "assets/prefabs/deployable/door barricades/door_barricade";

        object OnEntityTakeDamage(BaseCombatEntity entity, HitInfo info)
        {
            if (entity.name.Contains(barricadeFamily))
                entity.Kill(BaseNetworkable.DestroyMode.Gib);
            return null;
        }

    }
}