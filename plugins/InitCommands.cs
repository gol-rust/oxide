﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Oxide.Plugins
{
    [Info("InitCommands", "shinnova", "1.0.0")]
    [Description("Run commands on server start after a set interval")]
    public class InitCommands : CovalencePlugin
    {
        #region Configuration
        Configuration config;
        public class Configuration
        {
            [JsonProperty(PropertyName = "Run commands after x seconds")]
            public int SecondsToRun { get; set; } = 120;

            [JsonProperty(PropertyName = "Command list")]
            public List<string> CommandList { get; set; } = new List<string>();
        }

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                config = Config.ReadObject<Configuration>();
                if (config == null)
                    LoadDefaultConfig();
            }
            catch
            {
                LoadDefaultConfig();
            }
            SaveConfig();
        }

        protected override void LoadDefaultConfig()
        {
            LogWarning("Generating new configuration file");
            config = new Configuration();
        }

        protected override void SaveConfig() => Config.WriteObject(config);
        #endregion

        #region Functions
        void RunCommands()
        {
            if (config.CommandList.Count > 0)
            {
                foreach (string CommandToRun in config.CommandList)
                    server.Command(CommandToRun);
            }
            else
                return;
        }

        void OnServerInitialized()
        {
            timer.Once(config.SecondsToRun, RunCommands);
        }
        #endregion
    }
}