﻿using Oxide.Core;
using Oxide.Core.Configuration;
using Oxide.Core.Libraries.Covalence;
using System.Linq;
using System.Collections.Generic;
namespace Oxide.Plugins
{
    [Info("Enforced MOTD", "The Noxes", "0.0.3")]
    [Description("Enforce message on logon")]
    class enforcedmotd : CovalencePlugin
    {
        DynamicConfigFile dataFile = Interface.Oxide.DataFileSystem.GetDatafile("emotdusers");
        
        void OnPlayerSleepEnded(BasePlayer player)
        {
            
            if (dataFile[player.IPlayer.Id] != null && player.IsAlive())
            {
                player.IPlayer.Message(string.Format("<color=#ce422b>Please read:</color>\n{0}\n\nType <color=#ffa500ff>/ireadthemessage</color> to dismiss.",dataFile[player.IPlayer.Id]));
            }
                       
        }

        void OnDoorOpened(Door door, BasePlayer player)
        {
            if (dataFile[player.IPlayer.Id] != null)
            {
                player.IPlayer.Message(string.Format("<color=#ce422b>Please read:</color>\n{0}\n\nType <color=#ffa500ff>/ireadthemessage</color> to dismiss.", dataFile[player.IPlayer.Id]));
                door.CloseRequest();
            }
        }

        [Command("emotd"), Permission("enforcedmotd.admin")]
        private void emotdCommand(IPlayer player, string command, string[] args)
        {
            if (args.Count() < 2){
                if (args[0] == "list"){
                    if (dataFile.Count() == 0){
                        player.Reply("No active eMOTDs.");
                        return;
                    }
                    foreach (var element in dataFile)
                    {
                        player.Reply(string.Format("{0}: {1}", players.FindPlayerById(element.Key).Name, element.Value));

                    }
                    return;
                }
                player.Reply("Incorrect amount of arguments. /emotd playername \"free text for player to read\"");
                return;
            }

            List<IPlayer> resultedplayers = players.FindPlayers(args[0]).ToList();
            if (resultedplayers.Count() == 0){
                player.Reply(string.Format("No players found! You tried: {0}",args[0]));
                return;
            }
            IPlayer curplayer = resultedplayers[0];

            if (dataFile[curplayer.Id] != null){
                player.Reply(string.Format("Replacing existing MOTD for {0}.\nOLD: {1}\nNew: {2}", curplayer.Name, dataFile[curplayer.Id], args[1]));

            }
            else{
                player.Reply(string.Format("Added MOTD for {0}: {1}", curplayer.Name, args[1]));
            }

            dataFile[curplayer.Id] = args[1];

            if(curplayer.IsConnected){
                curplayer.Message(string.Format("<color=#ce422b>Please read:</color>\n{0}\n\nType <color=#ffa500ff>/ireadthemessage</color> to dismiss.", args[1]));

            }

            dataFile.Save();
            LogToFile("warnings", string.Format("[{0}] {1} ({2}) gave {3} ({4}) the following warning:\n{5}", System.DateTime.Now.ToShortTimeString(), player.Name, player.Id, curplayer.Name, curplayer.Id, args[1]), this);
        }
        [Command("ireadthemessage")]
        private void emotdDismissCommand(IPlayer player, string command, string[] args)
        {
            if (dataFile[player.Id] != null)
            {
                dataFile.Remove(player.Id);
                player.Message("Thank you! Please read <color=#ffa500ff>/rules</color> or contact staff if you have any questions.");
                Puts(string.Format("{0} read the message!", player.Name));
                dataFile.Save();
                LogToFile("warnings", string.Format("[{0}] {1} ({2}) has received their warning", System.DateTime.Now.ToShortTimeString(), player.Name, player.Id), this);
                return;
            }
            else{
                player.Message("Error: Command not available.");
                return;
            }


        }
    }
}