using Oxide.Core;
using System.Collections.Generic;
using System;
using System.Linq;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("KeycardPocket", "The Noxes", "1.0.4")]
    [Description("Put keycards into an interdimensional pocket")]
    public class KeycardPocket : RustPlugin
    {
        string box = "assets/prefabs/deployable/woodenbox/woodbox_deployed.prefab";
        List<StorageContainer> Containers = new List<StorageContainer>();
        HashSet<int> Boxes = new HashSet<int>();

        class StoredData
        {
            public Dictionary<string, List<string>> Pockets = new Dictionary<string, List<string>>();

            public StoredData()
            {
            }
        }

        StoredData storedData;

        void SaveData() => Interface.Oxide.DataFileSystem.WriteObject("KeycardPocket", storedData);

        void OnNewSave(string filename)
        {
            storedData.Pockets.Clear();
            SaveData();
        }

        void Init()
        {
            storedData = Interface.Oxide.DataFileSystem.ReadObject<StoredData>("KeycardPocket");
        }

        object OnItemPickup(Item item, BasePlayer player)
        {
            List<string> keycards = new List<string> { "keycard_green", "keycard_blue", "keycard_red" };
            if (keycards.Any(item.ToString().Contains))
            {
                List<string> cardList = new List<string>();
                if (storedData.Pockets.ContainsKey(player.UserIDString))
                    cardList = storedData.Pockets[player.UserIDString];
                int holdingCards = cardList.Count();
                if (holdingCards < 12)
                {
                    string cardShortName = keycards.FirstOrDefault(x => item.ToString().Contains(x));
                    cardList.Add(string.Format("{0},{1}", cardShortName, item.condition));
                    storedData.Pockets[player.UserIDString] = cardList;
                    SaveData();
                    item.Remove();
                    SendReply(player, "The keycard was put in your <color=#ffa500ff>/pocket</color>");
                }
            }
            return null;
        }

        void OpenPocket(BasePlayer player)
        {
            Vector3 pos = player.transform.position;
            BaseEntity ent = GameManager.server.CreateEntity(box, new Vector3(pos.x, pos.y - 2000, pos.z));
            ent.Spawn();
            Boxes.Add(ent.GetHashCode());
            StorageContainer storage = ent.GetComponent<StorageContainer>();
            Containers.Add(storage);
            storage.inventory.playerOwner = player;
            if (storedData.Pockets.ContainsKey(player.UserIDString))
            {
                foreach (string cardInfo in storedData.Pockets[player.UserIDString])
                {
                    string[] splitInfo = cardInfo.Split(',');
                    string shortName = splitInfo[0];
                    int durability = Convert.ToInt32(splitInfo[1]);
                    Item item = ItemManager.CreateByPartialName(shortName, 1);
                    item.condition = durability;
                    item.MoveToContainer(storage.inventory);
                }
            }
            timer.In(0.25f, () =>
            {
                player.inventory.loot.StartLootingEntity(storage, false);
                player.inventory.loot.AddContainer(storage.inventory);
                player.inventory.loot.SendImmediate();
                player.ClientRPCPlayer(null, player, "RPC_OpenLootPanel", storage.panelName);
            });
        }

        void OnItemAddedToContainer(ItemContainer container, Item item)
        {
            if (Containers.Exists(c => c.inventory == container))
            {
                BasePlayer player = container.playerOwner;
                timer.In(0.25f, () =>
                {
                    if (!item.ToString().Contains("keycard") && !item.ToString().Contains("fuse"))
                    {
                        if (!item.MoveToContainer(player.inventory.containerMain))
                            item.Drop(player.transform.position, new Vector3(0, 1, 0));
                        SendReply(player, string.Format("You could not fit the {0} in your pocket", item.info.displayName.english.ToLower()));
                    }
                });
            }
        }

        void OnLootEntityEnd(BasePlayer player, BaseCombatEntity entity)
        {
            if (Boxes.Contains(entity.GetHashCode()))
            {
                StorageContainer storage = entity as StorageContainer;
                List<string> cardList = new List<string>();
                foreach (Item item in storage.inventory.itemList)
                {
                    string itemShortName = null;
                    if (item.ToString().Contains("fuse"))
                        itemShortName = "fuse";
                    if (item.ToString().Contains("green"))
                        itemShortName = "keycard_green";
                    if (item.ToString().Contains("blue"))
                        itemShortName = "keycard_blue";
                    if (item.ToString().Contains("red"))
                        itemShortName = "keycard_red";
                    cardList.Add(string.Format("{0},{1}", itemShortName, item.condition));
                }
                storedData.Pockets[player.UserIDString] = cardList;
                SaveData();
                Boxes.Remove(entity.GetHashCode());
                Containers.Remove(storage);
                entity.Kill(BaseNetworkable.DestroyMode.None);
            }
        }

        [ChatCommand("pocket")]
        void pocketCommand(BasePlayer player, string command, string[] args)
        {
            OpenPocket(player);
        }
    }
}