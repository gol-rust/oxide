﻿namespace Oxide.Plugins
{
    [Info("No Chutes", "shinnova", "1.0.0")]
    [Description("")]
    public class NoChutes : RustPlugin
    {
        void OnServerInitialized()
        {
            foreach(BaseEntity entity in BaseNetworkable.serverEntities)
            {
                if (entity.PrefabName == "assets/prefabs/misc/parachute/parachute.prefab")
                    entity.Kill(BaseNetworkable.DestroyMode.None);
            }
        }
    }
}