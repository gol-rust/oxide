using System.Collections.Generic;
using System.Collections;
using System;
using Oxide.Core;
using System.Linq;

namespace Oxide.Plugins
{
    [Info("GOLPoll", "The Noxes", "2.1.8")]
    [Description("Lets people vote on polls")]

    public class GOLPoll : RustPlugin
    {
        #region Variables
        const string NoPerm = "golpoll.novote";
        const string AdminPerm = "golpoll.admin";
        #endregion

        #region Configuration
        private class PluginConfig
        {
            public int MaxPolls = 5;
        }

        private PluginConfig config;

        new void LoadDefaultMessages()
        {
            lang.RegisterMessages(new Dictionary<string, string>
            {
                ["NotAllowed"] = "You do not have permission to use that command.",
                ["NotYetVoted"] = "You have not yet voted on the following poll(s)\n{0}\nRun <color=#ce422b>/poll vote #</color> for information",
                ["AlreadyVoted"] = "You have already voted",
                ["HowToVote"] = "You can vote by running <color=#ce422b>/poll vote {0} [#]</color>\n<color=#ffa500ff>{1}</color>\n{2}",
                ["VoteSuccess"] = "Your vote has been added!",
                ["FormattedPoll"] = "Poll #{0}\n<color=#ffa500ff>{1}</color>\n<color=#808080ff>{2}</color>\nUse <color=#ce422b>/poll vote {0} [#]</color> to vote",
                ["ZeroPolls"] = "There are currently no polls.",
                ["NowActive"] = "Currently active polls:",
                ["TooManyPolls"] = "There are too many polls stored, please use <color=#ce422b>/poll remove</color> to remove polls.",
                ["NeedOptions"] = "You need at least 2 options on your poll.\nUsage: <color=#ce422b>/poll create</color> <color=#ffa500ff>{Question}</color> <color=#808080ff>{Option1} {Option2} ...</color>",
                ["CreateSuccess"] = "Poll created successfully!\n<color=#ffa500ff>{0}</color>\n{1}",
                ["NeedPollID"] = "Please provide a poll ID:\n{0}",
                ["AllRemoved"] = "All polls have been removed!",
                ["RemoveSuccess"] = "Poll #{0} successfully removed!",
                ["RemoveFail"] = "Could not remove poll #{0}. Does it exist?",
                ["NoVotes"] = "There are no registered votes for poll #{0}",
                ["PollResults"] = "<color=#ffa500ff>{0}</color>\n{1}",
                ["ActiveState"] = "{0} active: {1}",
                ["NotAPoll"] = "That poll does not exist",
                ["RegularHelp"] = "<color=#ce422b>/poll</color> - See all currently active polls\n<color=#ce422b>/poll vote [ID] [#]</color> - Vote on a chosen poll",
                ["AdminHelp"] = "<color=#ce422b>/poll create {Question} {Option1} {Option2} ...</color> - Create a poll\n<color=#ce422b>/poll results [ID]</color> - See the results of a poll\n<color=#ce422b>/poll toggle [ID]</color> - Toggle polls' active state\n<color=#ce422b>/poll remove all/[ID]</color> - Remove all/chosen polls from the data file"
            }, this);
        }
        #endregion

        #region Data
        public class StoredData
        {
            public Dictionary<int, PollInfo> polls = new Dictionary<int, PollInfo>();

            StoredData(){}
        }

        StoredData storedData;

        public class PollInfo
        {
            public bool Ongoing;
            public string Question;
            public Dictionary<string, List<string>> Options = new Dictionary<string, List<string>>();

            public PollInfo()
            {
            }

            public PollInfo(string Question, List<string> optionsList)
            {
                Ongoing = true;
                this.Question = Question;
                List<string> voterList = new List<string>();
                foreach(string option in optionsList)
                {
                    Options.Add(option, voterList);
                }
            }

            public void ToggleOngoing()
            {
                Ongoing = !Ongoing;
            }

            public void AddVote(int vote, string player)
            {
                Dictionary<int, string> tmpDict = new Dictionary<int, string>();
                int i = 1;
                foreach(string option in Options.Keys)
                {
                    tmpDict.Add(i, option);
                    i += 1;
                }
                string chosenOption = tmpDict[vote];
                List<string> voterList = new List<string>(Options[chosenOption]);
                voterList.Add(player);
                Options[chosenOption] = voterList;
            }
        }

        PollInfo pollInfo;
        #endregion

        #region Functions
        void SaveData() => Interface.Oxide.DataFileSystem.WriteObject("GOLPoll", storedData);
        
        int GetPollId()
        {
            int LastID = storedData.polls.Count();
            int uid = LastID + 1;
            return uid;
        }

        string GetMessage(string key, string userId) => lang.GetMessage(key, this, userId);
        #endregion

        #region Hooks
        protected override void LoadDefaultConfig()
        {
            PrintWarning("Creating default configuration");

            config = new PluginConfig();
            Config.Clear();
            Config.WriteObject(config);
        }

        protected override void SaveConfig()
        {
            Config.WriteObject(config);
        }

        void Loaded()
        {
            config = Config.ReadObject<PluginConfig>();
            storedData = Interface.Oxide.DataFileSystem.ReadObject<StoredData>("GOLPoll");
            permission.RegisterPermission(AdminPerm, this);
            permission.RegisterPermission(NoPerm, this);
        }

        void OnPlayerInit(BasePlayer player)
        {
            if (!permission.UserHasPermission(player.UserIDString, NoPerm))
            {
                List<string> notVoted = new List<string>();
                foreach(int pollID in storedData.polls.Keys)
                {
                    storedData.polls.TryGetValue(pollID, out pollInfo);
                    if (pollInfo.Ongoing)
                    {
                        bool voted = false;
                        foreach(List<string> voters in pollInfo.Options.Values)
                        {
                            if (voters.Contains(player.UserIDString))
                                voted = true;
                        }
                        if (!voted)
                            notVoted.Add(string.Format("#{0}: <color=#ffa500ff>{1}</color>", pollID, pollInfo.Question));
                    }
                }
                if (notVoted.Count > 0)
                {
                    SendReply(player, string.Format(GetMessage("NotYetVoted", player.UserIDString), String.Join("\n", notVoted.ToArray())));
                    return;
                }
            }
        }
        #endregion

        #region Commands
        [ChatCommand("poll")]
        void pollCommand(BasePlayer player, string command, string[] args)
        {
            List<string> argsList = new List<string>(args);
            if (permission.UserHasPermission(player.UserIDString, NoPerm))
            {
                SendReply(player, GetMessage("NotAllowed", player.UserIDString));
                return;
            }
            if (argsList.Count == 0)
            {
                List<string> activePolls = new List<string>();
                foreach(int pollID in storedData.polls.Keys)
                {
                    storedData.polls.TryGetValue(pollID, out pollInfo);
                    if (pollInfo.Ongoing)
                    {
                        List<string> optionList = new List<string>();
                        int count = 1;
                        foreach(string option in pollInfo.Options.Keys)
                        {
                            optionList.Add(string.Format("{0}. {1}", count, option));
                            count += 1;
                        }
                        activePolls.Add(string.Format(GetMessage("FormattedPoll", player.UserIDString), pollID, pollInfo.Question, String.Join("\n", optionList.ToArray())));
                    }
                }
                if (activePolls.Count == 0)
                {
                    SendReply(player, GetMessage("ZeroPolls", player.UserIDString));
                }
                else
                {
                    SendReply(player, GetMessage("NowActive", player.UserIDString));
                    foreach(string formattedPoll in activePolls)
                        SendReply(player, formattedPoll);
                }
                return;
            }
            switch (argsList[0].ToLower())
            {
                case "create":
                    {
                        argsList.RemoveAt(0);
                        if (!permission.UserHasPermission(player.UserIDString, AdminPerm))
                        {
                            SendReply(player, GetMessage("NotAllowed", player.UserIDString));
                            return;
                        }
                        if (storedData.polls.Count >= config.MaxPolls)
                        {
                            SendReply(player, GetMessage("TooManyPolls", player.UserIDString));
                            return;
                        }
                        if (argsList.Count < 3)
                        {
                            SendReply(player, GetMessage("NeedOptions", player.UserIDString));
                            return;
                        }
                        string question = argsList[0];
                        argsList.RemoveAt(0);
                        List<string> optionsList = new List<string>();
                        foreach (string arg in argsList)
                        {
                            optionsList.Add(arg);
                        }
                        pollInfo = new PollInfo(question, optionsList);
                        storedData.polls.Add(GetPollId(), pollInfo);
                        SaveData();
                        SendReply(player, string.Format(GetMessage("CreateSuccess", player.UserIDString), question, String.Join("\n", optionsList.ToArray())));
                        return;
                    }
                case "remove":
                    {
                        if (!permission.UserHasPermission(player.UserIDString, AdminPerm))
                        {
                            SendReply(player, GetMessage("NotAllowed", player.UserIDString));
                            return;
                        }
                        argsList.RemoveAt(0);
                        if (argsList.Count == 0)
                        {
                            List<string> allPolls = new List<string>();
                            foreach (int pollID in storedData.polls.Keys)
                            {
                                storedData.polls.TryGetValue(pollID, out pollInfo);
                                if (pollInfo.Ongoing)
                                    allPolls.Add(string.Format("<color=#008000ff>{0}</color> - {1}", pollID, pollInfo.Question));
                                else
                                    allPolls.Add(string.Format("<color=#ff0000ff>{0}</color> - {1}", pollID, pollInfo.Question));
                            }
                            if (allPolls.Count == 0)
                                SendReply(player, GetMessage("ZeroPolls", player.UserIDString));
                            else
                                SendReply(player, string.Format(GetMessage("NeedPollID", player.UserIDString), String.Join("\n", allPolls.ToArray())));
                            return;
                        }
                        if (argsList[0] == "all")
                        {
                            storedData.polls.Clear();
                            SaveData();
                            SendReply(player, GetMessage("AllRemoved", player.UserIDString));
                            return;
                        }
                        foreach (string arg in argsList)
                        {
                            int pollID = Convert.ToInt32(arg);
                            if (storedData.polls.Remove(pollID))
                            {
                                SaveData();
                                SendReply(player, string.Format(GetMessage("RemoveSuccess", player.UserIDString), pollID));
                            }
                            else
                                SendReply(player, string.Format(GetMessage("RemoveFail", player.UserIDString), pollID));
                        }
                        return;
                    }
                case "results":
                    {
                        if (!permission.UserHasPermission(player.UserIDString, AdminPerm))
                        {
                            SendReply(player, GetMessage("NotAllowed", player.UserIDString));
                            return;
                        }
                        argsList.RemoveAt(0);
                        if (argsList.Count == 0)
                        {
                            List<string> pollList = new List<string>();
                            foreach (int pollID in storedData.polls.Keys)
                            {
                                storedData.polls.TryGetValue(pollID, out pollInfo);
                                pollList.Add(string.Format("{0} - {1}", pollID, pollInfo.Question));
                            }
                            SendReply(player, string.Format(GetMessage("NeedPollID", player.UserIDString), String.Join("\n", pollList.ToArray())));
                            return;
                        }
                        else
                        {
                            int pollID = Convert.ToInt32(argsList[0]);
                            storedData.polls.TryGetValue(pollID, out pollInfo);
                            int totalCount = 0;
                            List<int> voteAmounts = new List<int>();
                            List<string> resultList = new List<string>();
                            int i = 0;
                            foreach (List<string> voterList in pollInfo.Options.Values)
                            {
                                voteAmounts.Add(voterList.Count);
                                totalCount += voterList.Count;
                            }
                            if (totalCount == 0)
                            {
                                SendReply(player, string.Format(GetMessage("NoVotes", player.UserIDString), pollID));
                                return;
                            }
                            foreach (string option in pollInfo.Options.Keys)
                            {
                                int votePercent = 0;
                                if (voteAmounts[i] != 0)
                                    votePercent = (int)Math.Round(voteAmounts[i] / ((float)totalCount / 100));
                                resultList.Add(string.Format("{0}% - {1}", votePercent, option));
                                i += 1;
                            }
                            SendReply(player, string.Format(GetMessage("PollResults", player.UserIDString), pollInfo.Question, String.Join("\n", resultList.ToArray())));
                            return;
                        }
                    }
                case "toggle":
                    {
                        if (!permission.UserHasPermission(player.UserIDString, AdminPerm))
                        {
                            SendReply(player, GetMessage("NotAllowed", player.UserIDString));
                            return;
                        }
                        argsList.RemoveAt(0);
                        if (argsList.Count == 0)
                        {
                            List<string> allPolls = new List<string>();
                            foreach (int pollID in storedData.polls.Keys)
                            {
                                storedData.polls.TryGetValue(pollID, out pollInfo);
                                if (pollInfo.Ongoing)
                                    allPolls.Add(string.Format("<color=#008000ff>{0}</color> - {1}", pollID, pollInfo.Question));
                                else
                                    allPolls.Add(string.Format("<color=#ff0000ff>{0}</color> - {1}", pollID, pollInfo.Question));
                            }
                            if (allPolls.Count == 0)
                                SendReply(player, GetMessage("ZeroPolls", player.UserIDString));
                            else
                                SendReply(player, string.Format(GetMessage("NeedPollID", player.UserIDString), String.Join("\n", allPolls.ToArray())));
                            return;
                        }
                        foreach (string arg in argsList)
                        {
                            int pollID = Convert.ToInt32(arg);
                            storedData.polls.TryGetValue(pollID, out pollInfo);
                            pollInfo.ToggleOngoing();
                            storedData.polls[pollID] = pollInfo;
                            SendReply(player, string.Format(GetMessage("ActiveState", player.UserIDString), pollID, pollInfo.Ongoing));
                        }
                        SaveData();
                        return;
                    }
                case "vote":
                    {
                        argsList.RemoveAt(0);
                        if (argsList.Count == 0)
                        {
                            List<string> activePolls = new List<string>();
                            foreach (int pollID in storedData.polls.Keys)
                            {
                                storedData.polls.TryGetValue(pollID, out pollInfo);
                                if (pollInfo.Ongoing)
                                    activePolls.Add(string.Format("{0} - {1}", pollID, pollInfo.Question));
                            }
                            SendReply(player, string.Format(GetMessage("NeedPollID", player.UserIDString), String.Join("\n", activePolls.ToArray())));
                            return;
                        }
                        if (argsList.Count == 1)
                        {
                            int count = 1;
                            int pollID = Convert.ToInt32(argsList[0]);
                            if (storedData.polls.TryGetValue(pollID, out pollInfo))
                            {
                                string question = pollInfo.Question;
                                List<string> optionsList = new List<string>();
                                foreach (string option in pollInfo.Options.Keys)
                                {
                                    optionsList.Add(string.Format("{0}. {1}", count, option));
                                    count += 1;
                                }
                                SendReply(player, string.Format(GetMessage("HowToVote", player.UserIDString), pollID, question, String.Join("\n", optionsList.ToArray())));
                                return;
                            }
                            SendReply(player, GetMessage("NotAPoll", player.UserIDString));
                            return;
                        }
                        else
                        {
                            bool alreadyVoted = false;
                            int pollID = Convert.ToInt32(argsList[0]);
                            if (storedData.polls.TryGetValue(pollID, out pollInfo))
                            {
                                int vote = Convert.ToInt32(argsList[1]);
                                foreach (List<string> optionList in pollInfo.Options.Values)
                                {
                                    if (optionList.Contains(player.UserIDString))
                                    {
                                        alreadyVoted = true;
                                        SendReply(player, GetMessage("AlreadyVoted", player.UserIDString));
                                        break;
                                    }
                                }
                                if (!alreadyVoted)
                                {
                                    pollInfo.AddVote(vote, player.UserIDString);
                                    storedData.polls[pollID] = pollInfo;
                                    SaveData();
                                    SendReply(player, GetMessage("VoteSuccess", player.UserIDString));
                                    return;
                                }
                            }
                            else
                            {
                                SendReply(player, GetMessage("NotAPoll", player.UserIDString));
                                return;
                            }
                        }
                        return;
                    }
                default:
                    {
                        if (!permission.UserHasPermission(player.UserIDString, AdminPerm))
                            SendReply(player, GetMessage("RegularHelp", player.UserIDString));
                        else
                            SendReply(player, string.Format("{0}\n{1}", GetMessage("RegularHelp", player.UserIDString), GetMessage("AdminHelp", player.UserIDString)));
                        return;
                    }
            }
        }

        [ConsoleCommand("poll")]
        void cpollCommand(ConsoleSystem.Arg arg)
        {
            if (arg.Connection == null)
            {
                List<string> allPolls = new List<string>();
                foreach (int pollID in storedData.polls.Keys)
                {
                    storedData.polls.TryGetValue(pollID, out pollInfo);
                    if (pollInfo.Ongoing)
                        allPolls.Add(string.Format("{0} - {1} (Active)", pollID, pollInfo.Question));
                    else
                        allPolls.Add(string.Format("{0} - {1} (Inactive)", pollID, pollInfo.Question));
                }

                if (arg.Args == null)
                {
                    Puts(string.Format("{0}\npoll create \"Question\" \"Option\" \"Option\" ... - Create a poll\npoll results [ID] - See the results of a poll\npoll toggle [ID] - Toggle polls' active state\npoll remove all/[ID] - Remove all/chosen polls from the data file", String.Join("\n", allPolls.ToArray())));
                    return;
                }
                switch (arg.Args[0].ToLower())
                {
                    case "create":
                        {
                            if (arg.Args.Length < 4)
                            {
                                Puts("Usage: poll create \"Question\" \"Option\" \"Option\" ...");
                                return;
                            }
                            else
                            {
                                string question = arg.Args[1];
                                List<string> optionList = new List<string>(arg.Args);
                                optionList.RemoveRange(0, 2);
                                pollInfo = new PollInfo(question, optionList);
                                storedData.polls.Add(GetPollId(), pollInfo);
                                SaveData();
                                Puts(string.Format("Poll created successfully!\n{0}\n{1}", question, String.Join("\n", optionList.ToArray())));
                                return;
                            }
                        }
                    case "remove":
                        {
                            if (storedData.polls.Count() == 0)
                            {
                                Puts("There are currently no polls.");
                                return;
                            }
                            if (arg.Args.Length < 2)
                            {
                                Puts(string.Format("Please run \"poll remove all\" or provide the IDs you wish to remove:\n{0}", allPolls.ToArray()));
                                return;
                            }
                            if (arg.Args[1] == "all")
                            {
                                storedData.polls.Clear();
                                SaveData();
                                Puts("All polls have been removed!");
                                return;
                            }
                            List<string> pollList = new List<string>(arg.Args);
                            pollList.RemoveAt(0);
                            foreach (string i in pollList)
                            {
                                int pollID = Convert.ToInt32(i);
                                if (storedData.polls.Remove(pollID))
                                {
                                    SaveData();
                                    Puts(string.Format("Poll #{0} successfully removed!", pollID));
                                }
                                else
                                    Puts(string.Format("Could not remove poll #{0}. Does it exist?", pollID));
                            }
                            return;
                        }
                    case "toggle":
                        {
                            if (storedData.polls.Count() == 0)
                            {
                                Puts("There are currently no polls.");
                                return;
                            }
                            if (arg.Args.Length < 2)
                            {
                                Puts(string.Format("Please provide the IDs you wish to toggle:\n{0}", allPolls.ToArray()));
                                return;
                            }
                            List<string> pollList = new List<string>(arg.Args);
                            pollList.RemoveAt(0);
                            foreach (string i in pollList)
                            {
                                int pollID = Convert.ToInt32(i);
                                storedData.polls.TryGetValue(pollID, out pollInfo);
                                pollInfo.ToggleOngoing();
                                storedData.polls[pollID] = pollInfo;
                                Puts(string.Format("{0} active: {1}", pollID, pollInfo.Ongoing));
                            }
                            SaveData();
                            return;
                        }
                    case "results":
                        {
                            if (storedData.polls.Count() == 0)
                            {
                                Puts("There are currently no polls.");
                                return;
                            }
                            if (arg.Args.Length < 2)
                            {
                                Puts(string.Format("Please provide one of the following IDs:\n{0}", allPolls.ToArray()));
                                return;
                            }
                            else
                            {
                                int pollID = Convert.ToInt32(arg.Args[1]);
                                storedData.polls.TryGetValue(pollID, out pollInfo);
                                int totalCount = 0;
                                List<int> voteAmounts = new List<int>();
                                List<string> resultList = new List<string>();
                                int i = 0;
                                foreach (List<string> voterList in pollInfo.Options.Values)
                                {
                                    voteAmounts.Add(voterList.Count);
                                    totalCount += voterList.Count;
                                }
                                if (totalCount == 0)
                                {
                                    Puts(string.Format("There are no registered votes for poll #{0}", pollID));
                                    return;
                                }
                                foreach (string option in pollInfo.Options.Keys)
                                {
                                    int votePercent = 0;
                                    if (voteAmounts[i] != 0)
                                        votePercent = (int)Math.Round(voteAmounts[i] / ((float)totalCount / 100));
                                    resultList.Add(string.Format("{0}% - {1}", votePercent, option));
                                    i += 1;
                                }
                                Puts(string.Format("{0}\n{1}", pollInfo.Question, String.Join("\n", resultList.ToArray())));
                                return;
                            }
                        }
                    default:
                        {
                            Puts("poll create \"Question\" \"Option\" \"Option\" ... - Create a poll\npoll results [ID] - See the results of a poll\npoll toggle [ID] - Toggle polls' active state\npoll remove all/[ID] - Remove all/chosen polls from the data file");
                            return;
                        }
                }
            }
        }
        #endregion
    }
}