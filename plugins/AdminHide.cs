﻿using System.Collections.Generic;
using System.Linq;
using Oxide.Core.Libraries.Covalence;
using Oxide.Core.Plugins;
using Oxide.Game.Rust.Cui;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("AdminHide", "The Noxes", "0.9.8")]
    [Description("Allows admins to hide as props in game")]
    public class AdminHide : CovalencePlugin
    {
        [PluginReference]
        Plugin Vanish, Freeze;
        #region Localization

        protected override void LoadDefaultMessages()
        {
            lang.RegisterMessages(new Dictionary<string, string>()
            {
                ["AlreadyHidden"] = "You are already hidden",
                ["Hiding"] = "You are hiding... shhh!",
                ["NotAllowed"] = "You are not allowed to use the '{0}' command",
                ["NotHiding"] = "You are no longer hiding."
            }, this);
        }

        #endregion
 
        #region Initialization

        private Dictionary<IPlayer, string> props = new Dictionary<IPlayer, string>();
        private Dictionary<IPlayer, BaseEntity> hiddenProps = new Dictionary<IPlayer, BaseEntity>();

        private const string permAllow = "adminhide.allow";

        private void Init()
        {
            permission.RegisterPermission(permAllow, this);
        }

        #endregion

        #region Player Hiding

        private void OnPlayerInput(BasePlayer player, InputState input)
        {
            var iplayer = players.FindPlayerById(player.UserIDString);
            if (props.ContainsKey(iplayer) && input.WasJustPressed(BUTTON.FIRE_PRIMARY))
            {
                if (hiddenProps.ContainsKey(iplayer))
                    UnhidePlayer(iplayer);
                else
                    HidePlayer(iplayer);
            }
            if (props.ContainsKey(iplayer) && !hiddenProps.ContainsKey(iplayer) && input.WasJustPressed(BUTTON.FIRE_SECONDARY))
                SelectProp(iplayer);
            if (hiddenProps.ContainsKey(iplayer) && input.IsDown(BUTTON.FIRE_SECONDARY))
                if (input.IsDown(BUTTON.SPRINT))
                    RotatePropLeft(iplayer);
                else
                    RotatePropRight(iplayer);
        }

        private void SelectProp(IPlayer player)
        {
            var basePlayer = player.Object as BasePlayer;
            var ray = new Ray(basePlayer.eyes.position, basePlayer.eyes.HeadForward());
            var entity = FindObject(ray, 3);
            string newProp = "assets/prefabs/player/player/player.prefab";
            if (entity != null)
                newProp = entity.name;
            if (props.ContainsKey(player))
                props[player] = newProp;
            else
                props.Add(player, newProp);
            string entityName = newProp.Substring(newProp.LastIndexOf("/")+1);
            player.Reply(string.Format("You have chosen <color=#ffa500ff>{0}</color>", entityName));
        }

        private void RotatePropLeft(IPlayer player)
        {
            var entity = hiddenProps[player];
            entity.transform.Rotate(0, -2, 0);
            entity.SendNetworkUpdate();
        }

        private void RotatePropRight(IPlayer player)
        {
            var entity = hiddenProps[player];
            entity.transform.Rotate(0, 2, 0);
            entity.SendNetworkUpdate();
        }

        private void HidePlayer(IPlayer player)
        {
            if (hiddenProps.ContainsKey(player)) hiddenProps.Remove(player);

            var basePlayer = player.Object as BasePlayer;
            var playerEyesPos = basePlayer.eyes.position;
            var playerBodyPos = basePlayer.transform.position;
            Vector3 downwards = new Vector3(0, -1, 0);
            var ray = new Ray(playerEyesPos, downwards);
            var distanceObject = FindDistance(ray, 10);
            float terrainHeight = TerrainMeta.HeightMap.GetHeight(playerBodyPos);
            float objectHeight = terrainHeight;
            if (playerBodyPos.y >= terrainHeight + 0.1 || playerBodyPos.y <= terrainHeight - 0.1)
                if (distanceObject != null)
                    objectHeight = playerEyesPos.y - distanceObject;
                else
                    objectHeight = playerBodyPos.y;
            Vector3 propPos = new Vector3(playerBodyPos.x, objectHeight, playerBodyPos.z);
            string entity = props[player];

            // Create the prop entity
            var prop = GameManager.server.CreateEntity(entity, propPos, basePlayer.transform.rotation);
            prop.SendMessage("SetDeployedBy", basePlayer, SendMessageOptions.DontRequireReceiver);
            prop.SendMessage("InitializeItem", entity, SendMessageOptions.DontRequireReceiver);
            prop.Spawn();
            hiddenProps.Add(player, prop);

            // Make the player invisible
            basePlayer.gameObject.SetLayerRecursive(10);
            basePlayer.CancelInvoke("MetabolismUpdate");
            basePlayer.CancelInvoke("InventoryUpdate");
            if (!IsInvisible(basePlayer))
            {
                Effect.server.Run("assets/prefabs/npc/patrol helicopter/effects/rocket_fire.prefab", playerBodyPos);
                Vanish?.Call("Disappear", basePlayer);
            }
            Freeze?.Call("FreezePlayer", player);
        }

        private void UnhidePlayer(IPlayer player)
        {
            if (!hiddenProps.ContainsKey(player)) return;

            // Remove the prop entity
            var prop = hiddenProps[player];
            if (!prop.IsDestroyed) prop.Kill(BaseNetworkable.DestroyMode.None);
            hiddenProps.Remove(player);

            // Make the player visible
            var basePlayer = player.Object as BasePlayer;
            basePlayer.metabolism.Reset();
            basePlayer.InvokeRepeating("InventoryUpdate", 1f, 0.1f * Random.Range(0.99f, 1.01f));
            basePlayer.gameObject.SetLayerRecursive(17);
            if (IsInvisible(basePlayer))
            {
                Effect.server.Run("assets/prefabs/npc/patrol helicopter/effects/rocket_fire.prefab", basePlayer.transform.position);
                Vanish?.Call("Reappear", basePlayer);
            }
            Freeze?.Call("UnfreezePlayer", player);
        }

        #endregion

        #region Chat Commands

        [Command("hide")]
        private void HideCommand(IPlayer player, string command, string[] args)
        {
            var basePlayer = player.Object as BasePlayer;
            Puts(string.Format("{0}", basePlayer.transform.rotation));
            if (!player.HasPermission(permAllow))
            {
                player.Reply(Lang("NotAllowed", player.Id, command));
                return;
            }

            if (hiddenProps.ContainsKey(player))
            {
                player.Reply(Lang("AlreadyHidden", player.Id));
                return;
            }

            player.Reply("Use rightclick to pick a new object.\nUse left click to hide/unhide.\nRightclick while hidden to rotate.");
            SelectProp(player);
        }

        [Command("unhide")]
        private void UnhideCommand(IPlayer player, string command, string[] args)
        {
            if (!player.HasPermission(permAllow))
            {
                player.Reply(Lang("NotAllowed", player.Id, command));
                return;
            }

            UnhidePlayer(player);
            props.Remove(player);
            player.Reply("You are no longer playing");
        }

        #endregion

        #region Cleanup Props

        private void OnEntityKill(BaseEntity entity)
        {
            foreach(var hidden in hiddenProps)
            {
                BaseEntity prop = hidden.Value;
                IPlayer player = hidden.Key;
                if (prop == entity)
                {
                    UnhidePlayer(player);
                    props.Remove(player);
                    player.Reply("You have been found. Game over.");
                }
            }
        }

        private void Unload()
        {
            var propList = hiddenProps.Keys.ToList();
            foreach (var prop in propList) UnhidePlayer(prop);
        }

        #endregion

        #region Helper Methods

        private static BaseEntity FindObject(Ray ray, float distance)
        {
            RaycastHit hit;
            return !Physics.Raycast(ray, out hit, distance) ? null : hit.GetEntity();
        }

        private static float FindDistance(Ray ray, float height)
        {
            RaycastHit hit;
            Physics.Raycast(ray, out hit, height);
            return hit.distance;
        }

        public bool IsInvisible(BasePlayer player)
		{
			object invisible = Vanish?.Call("IsInvisible", player);
			if (invisible is bool)
			{
				if ((bool)invisible)
					return true;
			}
			return false;
		}

        private string Lang(string key, string id = null, params object[] args) => string.Format(lang.GetMessage(key, this, id), args);

        #endregion
    }
}
