using System;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("ObjectInfo", "The Noxes", "1.1.8")]
    [Description("Look at an object and get its information")]
    public class ObjectInfo : RustPlugin
    {
        const string permAllow = "objectinfo.use";
        const string permSkin = "objectinfo.skin";

        void Init()
        {
            permission.RegisterPermission(permAllow, this);
            permission.RegisterPermission(permSkin, this);
        }

        void RetrieveInfo(BasePlayer player)
        {
            var ray = new Ray(player.eyes.position, player.eyes.HeadForward());
            var entity = FindObject(ray, 3);
            if (entity != null)
            {
                string skinStatus = "";
                if (entity.skinID != 0)
                    skinStatus = string.Format(" (using skin {0})", entity.skinID);
                string information = string.Format("{0}{1}:\nPosition:\n{2}\nRotation:\n{3}\nx {4}\ny {5}\nz {6}", entity.name, skinStatus, entity.transform.position, entity.transform.rotation, entity.transform.rotation.x, entity.transform.rotation.y, entity.transform.rotation.z);
                SendConsoleReply(player.net.connection, "[Object Info]");
                SendConsoleReply(player.net.connection, information);
                Puts(information);
                SendReply(player, "Information successfully sent to console");
                return;
            }
            SendReply(player, "Failed at getting object information");
        }

        [ChatCommand("getinfo")]
        void InfoCommand(BasePlayer player, string command, string[] args)
        {
            if (!permission.UserHasPermission(player.UserIDString, permAllow)) return;
            RetrieveInfo(player);
        }

        [ConsoleCommand("getinfo")]
        void cInfoCommand(ConsoleSystem.Arg arg)
        {
            if (arg.Connection != null)
                RetrieveInfo(arg.Player());
        }

        [ChatCommand("setskin")]
        void SkinCommand(BasePlayer player, string command, string[] args)
        {
            if (!permission.UserHasPermission(player.UserIDString, permSkin)) return;
            if (args.Length == 0)
            {
                SendReply(player, "Use <color=#ffa500ff>/setskin ID</color> to change to item in front of you, or <color=#ffa500ff>/setskin hand ID</color> to change your held item.");
                return;
            }
            if (args[0] == "hand")
            {
                if (args.Length < 2)
                {
                    SendReply(player, "You need to define a skin to use");
                    return;
                }
                Item toskin = player.GetActiveItem();
                BaseProjectile weapon = toskin.GetHeldEntity() as BaseProjectile;
                if (weapon != null && weapon.primaryMagazine.contents > 0)
                {
                    SendReply(player, "Please remove ammo/fuel before skinning items");
                    return;
                }
                Item skinned_item = ItemManager.Create(toskin.info, toskin.amount, (ulong)Convert.ToInt32(args[1]));
                skinned_item.condition = toskin.condition;
                if (toskin.contents != null)
                {
                    foreach (Item gunMod in toskin.contents.itemList)
                    {
                        Item clone = ItemManager.Create(gunMod.info, gunMod.amount);
                        clone.condition = gunMod.condition;
                        if (!clone.MoveToContainer(player.inventory.containerMain))
                            clone.Drop(player.transform.position, new Vector3(0, 1, 0));
                    }
                }
                toskin.Remove();
                BaseProjectile skinned_weapon = skinned_item.GetHeldEntity() as BaseProjectile;
                if (skinned_weapon != null)
                    skinned_weapon.primaryMagazine.contents = 0;
                timer.Once(1f, () =>
                {
                    skinned_item.MoveToContainer(player.inventory.containerBelt);
                    SendReply(player, string.Format("Successfully set the skin of {0} to {1}", skinned_item.info.shortname, args[1]));
                });
                return;
            }
            var ray = new Ray(player.eyes.position, player.eyes.HeadForward());
            var entity = FindObject(ray, 3);
            if (entity != null)
            {
                entity.skinID = (ulong)Convert.ToInt32(args[0]);
                entity.SendNetworkUpdateImmediate();
                SendReply(player, string.Format("Successfully set the skin of {0} to {1}", entity.ShortPrefabName, args[0]));
            }
        }

        [ChatCommand("renew")]
        void RenewCommand(BasePlayer player, string command, string[] args)
        {
            if (!permission.UserHasPermission(player.UserIDString, permAllow)) return;
            var ray = new Ray(player.eyes.position, player.eyes.HeadForward());
            var entity = FindObject(ray, 3);
            if (entity != null)
            {
                BaseEntity newEnt = GameManager.server.CreateEntity(entity.name, entity.transform.position, entity.transform.rotation);
                entity.Kill(BaseNetworkable.DestroyMode.None);
                newEnt.Spawn();
                string entityName = newEnt.name.Substring(newEnt.name.LastIndexOf("/") + 1);
                SendReply(player, string.Format("Successfully renewed {0}", entityName));
                return;
            }
            SendReply(player, "Could not renew anything");
            return;
        }
        
        BaseEntity FindObject(Ray ray, float distance)
        {
            RaycastHit hit;
            return !Physics.Raycast(ray, out hit, distance) ? null : hit.GetEntity();
        }

        void SendConsoleReply(Network.Connection cn, string msg)
        {
            if (Network.Net.sv.IsConnected())
            {
                Network.Net.sv.write.Start();
                Network.Net.sv.write.PacketID(Network.Message.Type.ConsoleMessage);
                Network.Net.sv.write.String(msg);
                Network.Net.sv.write.Send(new Network.SendInfo(cn));
            }
        }
    }
}