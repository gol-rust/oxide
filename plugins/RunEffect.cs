using Oxide.Core.Plugins;
using System;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("RunEffect", "The Noxes", "1.0")]
    [Description("Lets you run effects. Damnit Nox")]
    
    public class RunEffect : RustPlugin
    {
        private const string effectperm = "runeffect.use";

        void Init()
        {
            permission.RegisterPermission(effectperm, this);
        }

        [ChatCommand("effect")]
        void effect_command(BasePlayer player, string command, string[] args)
        {
            if (!permission.UserHasPermission(player.UserIDString, effectperm))
            {
                SendReply(player, "You don't have access to that command.");
                return;
            }
            if (args.Count == 0)
            {
                SendReply(player, "Please provide the effect you want to play");
                return;
            }
            else
            {
                Effect.server.Run(string.Format("\"{0}\"", args[0]), player.transform.position);
                return;
            }
        }
    }
}