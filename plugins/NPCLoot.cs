﻿using System.Linq;
using System.Collections.Generic; 
using Oxide.Core;
using Oxide.Core.Plugins;

namespace Oxide.Plugins
{
    [Info("NPCLoot", "Steenamaroo", "1.0.2", ResourceId = 0)] 

    [Description("Custom loot table plugin for NPCPlayer corpses.")]

    public class NPCLoot : RustPlugin
    {
        [PluginReference]
        Plugin BotSpawn;

        #region Vars   
        public System.Random random = new System.Random();
        bool loaded = false;
        List<string> lootTableNames = new List<string>();
        Dictionary<string, LootTable> lootTables = new Dictionary<string, LootTable>(); 

        Dictionary<string, Dictionary<int, string>> balancedCategories = new Dictionary<string, Dictionary<int, string>>(); 
        Dictionary<string, Dictionary<string, Dictionary<int, KeyValuePair<string, lootData>>>> balancedItems = new Dictionary<string, Dictionary<string, Dictionary<int, KeyValuePair<string, lootData>>>>();
        Dictionary<string, List<ulong>> BotSpawnBots = new Dictionary<string, List<ulong>>();

        public Dictionary<string, List<ulong>> BotTypes = new Dictionary<string, List<ulong>>();
        #endregion

        void OnServerInitialized() 
        {
            RefreshbotSpawn();
            LoadConfigVariables();
            foreach (var lT in lootTableNames) 
            {
                CreateLootTable(lT);
                BalanceCategories(lT);
                BalanceItems(lT);
            }
            loaded = true;      
            foreach (BaseEntity entity in BaseNetworkable.serverEntities.Where(entity => entity is NPCPlayerApex))
            {
                AddToBotList(entity as NPCPlayerApex);
            } 
        }

        #region BotHandling 
        void OnEntitySpawned(BaseEntity entity)
        {
            if (!loaded) return;
            timer.Once(0.1f, () => //let other plugs do their thing first.  
            {
                if (entity != null && entity is NPCPlayerApex)
                {
                    NPCPlayerApex nPC = entity as NPCPlayerApex;
                    RefreshbotSpawn();
                    AddToBotList(nPC); 
                }  
                Settings botType = new Settings(); 
                if (entity != null && entity is NPCPlayerCorpse)
                {
                    var corpse = entity as NPCPlayerCorpse; 
                    var path = configData.CorpseTypes;
                    bool foundMatch = false;
                    foreach (var entry in BotTypes.Where(entry => entry.Value.Contains(corpse.playerSteamID)))
                    {
                        botType = path[entry.Key];
                        foundMatch = true;
                    } 
                    if (!botType.enabled || !foundMatch) 
                        return;
                    corpse.containers[0].Clear();
                    MakeLoot(botType.lootTable, botType, corpse); 
                }  
            });
        } 
         
        void RefreshbotSpawn() 
        {
            if (BotSpawn)
                BotSpawnBots = (Dictionary<string, List<ulong>>)BotSpawn?.Call("BotSpawnBots");
        }

        void AddToBotList(NPCPlayerApex npc) 
        {
            foreach (var monument in BotSpawnBots.Where(monument => monument.Value.Contains(npc.userID)))  
            {
                AddBotSpawn(npc, monument.Key);
                return;
            } 
            if (npc is NPCMurderer)  
                BotTypes["Murderer"].Add(npc.userID);   
            else if (npc is Scientist)
            {   
                if (npc.ShortPrefabName.Contains("scientist_gunner")) 
                {
                    BotTypes["MountedScientist"].Add(npc.userID); 
                    return;
                }
                var spawner = npc.AiContext?.AiLocationManager;
                if (spawner == null)
                    BotTypes["Scientist-Other"].Add(npc.userID);
                else if (spawner.LocationType.ToString() == "JunkpileA" || spawner.LocationType.ToString() == "JunkpileG")
                    BotTypes["JunkPileScientists"].Add(npc.userID);
                else if (spawner.LocationType.ToString() == "MilitaryTunnels")
                    BotTypes["MilitaryTunnel"].Add(npc.userID);
                else if (spawner.LocationType.ToString() == "Compound")
                    BotTypes["PeaceKeeperScientist"].Add(npc.userID);
                else if (spawner.LocationType.ToString() == "BanditTown")
                    BotTypes["BanditTown"].Add(npc.userID); 
            }  
        }
                       
        void AddBotSpawn(NPCPlayer npc, string monument)
        {
            if (configData.GlobalSettings.corpseTypePerBotSpawnProfile)
            {
                if (BotTypes.ContainsKey("BotSpawn-" + monument))
                    BotTypes["BotSpawn-" + monument].Add(npc.userID);
                else
                    LoadConfigVariables(); 
            }
            else
                BotTypes["BotSpawn"].Add(npc.userID);
        }

        void OnEntityKill(BaseNetworkable entity) 
        {
            ulong userID = 0;
            if (entity is NPCPlayerApex)
            {
                NPCPlayerApex npc = entity as NPCPlayerApex;
                userID = npc.userID;
                timer.Once(0.2f, () => RemoveFromMemory(userID)); 
            }
        }

        void RemoveFromMemory(ulong userID)
        {
            foreach (var botList in BotTypes)
            {
                botList.Value.Remove(userID); break;
            }
        }
        #endregion

        #region LootHandling
        public void BalanceCategories(string lootTable)
        {
            int counter = 0;
            balancedCategories.Add(lootTable, new Dictionary<int, string>());
            foreach (var category in lootTables[lootTable].Categories)
            {
                if (!HasValidItem(lootTable, category.Key)) continue;
                for (int i = 0; i < category.Value.probability; i++)
                {
                    balancedCategories[lootTable].Add(counter, category.Key);
                    counter++;
                }
            }
        }

        public void BalanceItems(string lootTable) 
        {
            balancedItems.Add(lootTable, new Dictionary<string, Dictionary<int, KeyValuePair<string, lootData>>>());
            foreach (var category in lootTables[lootTable].LootTypes.Where(category => lootTables[lootTable].Categories[category.Key].probability > 0))
            {
                if (!HasValidItem(lootTable, category.Key)) continue;
                balancedItems[lootTable].Add(category.Key, new Dictionary<int, KeyValuePair<string, lootData>>());
                int counter = 0;
                foreach (var entry in category.Value)
                {
                    for (int i = 0; i < entry.Value.probability; i++)
                    {
                        var blank = new KeyValuePair<string, lootData>(entry.Key, entry.Value);
                        balancedItems[lootTable][category.Key].Add(counter, blank); 
                        counter++;
                    }
                }
            }
        }

        public bool HasValidItem(string lootTable, string category)
        {
            if (lootTables[lootTable].LootTypes[category].Count == 0) return false;
            bool hasEnabled = false;
            foreach (var item in lootTables[lootTable].LootTypes[category].Where(item => item.Value.probability > 0))
            {
                hasEnabled = true;
                break;
            }
            if (hasEnabled) return true; 
            return false;
        }

        public void MakeLoot(string lootTable, Settings settings, NPCPlayerCorpse corpse)
        {
            if (!settings.enabled) return;
            bool blueprint = false;
            int safety = 0;
            int number = settings.maxItems;
            List<Item> newLoot = new List<Item>();

            if (settings.maxItems > settings.minItems)
                number = random.Next(settings.minItems, settings.maxItems);
            for (int i = 0; i < number; i++)
            {
                safety++;
                bool duplicate = false;
                if (safety > 50) { break; }
                int chance = random.Next(100);
                string category = GetCategory(lootTable);
                Item item = GetItem(lootTable, category);
                if (item == new Item()) return;
                CategorySettings confPath = lootTables[lootTable].Categories[category];

                foreach (var newItem in newLoot)
                {
                    if (newItem.info.shortname == item.info.shortname && !(configData.GlobalSettings.allowDuplicates)) 
                        duplicate = true;
                    if (newItem.blueprintTarget != 0 && item.blueprintTarget != 0 && newItem.blueprintTarget == item.blueprintTarget && !(configData.GlobalSettings.allowDuplicates))
                        duplicate = true;
                }
                if (duplicate)
                { 
                    i--;
                    continue; 
                }
                if (!duplicate)
                {
                    if (category.ToString() == "Weapon" && !settings.noGuns)
                    {
                        Item itemToAdd = MakeBluePrint(item, chance, confPath);
                        newLoot.Add(itemToAdd);
                        if (itemToAdd.blueprintTarget != 0)
                            blueprint = true;

                        if (settings.gunsWithAmmo && !blueprint)
                        {
                            foreach (var gun in lootTables[lootTable].LootTypes["Weapon"].Where(gun => gun.Key == item.info.shortname.ToString()))
                            { 
                                string matchedAmmo;
                                if (ammoTypes.TryGetValue(gun.Key, out matchedAmmo))
                                {
                                    var ammopath = lootTables[lootTable].LootTypes["Resources"]["lowgradefuel"];
                                    int stack = random.Next(ammopath.minStack, ammopath.maxStack);
                                    if (gun.Key != "flamethrower")
                                    {
                                        ammopath = lootTables[lootTable].LootTypes["Ammunition"][matchedAmmo];
                                        stack = random.Next(ammopath.minStack, ammopath.maxStack);
                                    }
                                    if (stack > 10)
                                        stack = (stack / 10) * 10;

                                    if (ammopath.probability > 0)
                                    {
                                        Item ammoItem = ItemManager.CreateByName(matchedAmmo, stack);
                                        newLoot.Add(ammoItem);
                                        i++;
                                    } 
                                }
                            }
                        }
                    }
                    else if (category.ToString() != "Weapon")
                        newLoot.Add(MakeBluePrint(item, chance, confPath));
                }
                else
                    i--;
            }
            if (newLoot.Count > number)
                foreach (var entry in newLoot.Where(entry => entry.info.category.ToString() != "Weapon" && entry.info.category.ToString() != "Ammunition"))
                {
                    newLoot.Remove(entry);
                    break;
                }
            foreach (var entry in newLoot)
            {
                entry.MoveToContainer(corpse.containers[0], -1, true);
            }

            RemoveFromMemory(corpse.playerSteamID);
        }

        public string GetCategory(string lootTable)
        {
            int catnumber = random.Next(balancedCategories[lootTable].Count);
            return balancedCategories[lootTable][catnumber];
        }

        public Item GetItem(string lootTable, string category)
        { 
            Item item = new Item();
            int stack = 1;
            List<ulong> skins = new List<ulong>();
            ulong skin = 0;
            var chosenItem = random.Next(balancedItems[lootTable][category].Count);
            var path = balancedItems[lootTable][category][chosenItem];

            if (path.Value.minStack < path.Value.maxStack)
                stack = random.Next(path.Value.minStack, path.Value.maxStack);

            if (category == "Weapon" || category == "Attire") skins = path.Value.skins;
            if (category == "Resources" && stack > 10) stack = (stack / 10) * 10;

            if (skins.Count != 0)
                skin = skins[random.Next(skins.Count)];

            item = ItemManager.CreateByName(path.Key, stack, skin);
            return item;
        }

        Dictionary<string, string> ammoTypes = new Dictionary<string, string>()
        {
            { "flamethrower", "lowgradefuel" }, 
            { "pistol.nailgun", "ammo.nailgun.nails" }, 
            { "rocket.launcher", "ammo.rocket.basic" },
            { "multiplegrenadelauncher", "ammo.grenadelauncher.buckshot" },
            { "bow.hunting", "arrow.wooden" },{ "crossbow", "arrow.wooden" },
            { "pistol.eoka", "ammo.handmade.shell" },{ "shotgun.waterpipe", "ammo.handmade.shell" },
            { "shotgun.pump", "ammo.shotgun" },{ "shotgun.double", "ammo.shotgun" },{ "shotgun.spas12", "ammo.shotgun" },
            { "rifle.ak", "ammo.rifle" },{ "rifle.lr300", "ammo.rifle" },{ "rifle.semiauto", "ammo.rifle" },{ "rifle.bolt", "ammo.rifle" },{ "lmg.m249", "ammo.rifle" },
            { "pistol.revolver", "ammo.pistol" },{ "pistol.m92", "ammo.pistol" },{ "pistol.semiauto", "ammo.pistol" },{ "smg.thompson", "ammo.pistol" },{ "smg.mp5", "ammo.pistol" },{ "smg.2", "ammo.pistol" },
        };

        public Item MakeBluePrint(Item item, int chance, CategorySettings confPath)
        {
            ItemBlueprint itemBlueprint = ItemManager.FindItemDefinition(item.info.shortname).Blueprint;
            if (confPath.allowBlueprints && itemBlueprint != null && itemBlueprint.isResearchable && chance < confPath.blueprintChancePercent)
                item.info.spawnAsBlueprint = true;

            return item;
        }
        #endregion

        #region Data
        public class LootTable
        {
            public bool allowChristmas = false;
            public bool allowKeycards = false;
            
            public List<string> BlackList = new List<string>(); 

            public static CategorySettings CategorySettings = new CategorySettings();
            public Dictionary<string, CategorySettings> Categories = new Dictionary<string, CategorySettings>
            {
                { "Ammunition", CategorySettings }, { "Attire", CategorySettings }, { "Component", CategorySettings }, { "Construction", CategorySettings }, { "Food", CategorySettings }, { "Items", CategorySettings },
                { "Medical", CategorySettings }, { "Misc", CategorySettings }, { "Tool", CategorySettings }, { "Traps", CategorySettings }, { "Resources", CategorySettings }, { "Weapon", CategorySettings }, { "Electrical", CategorySettings }, { "Fun", CategorySettings }
            };

            public static Dictionary<string, lootData> Ammunition = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Attire = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Component = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Construction = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Food = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Items = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Medical = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Misc = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Tool = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Traps = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Resources = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Weapon = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Electrical = new Dictionary<string, lootData>();
            public static Dictionary<string, lootData> Fun = new Dictionary<string, lootData>();

            public Dictionary<string, Dictionary<string, lootData>> LootTypes = new Dictionary<string, Dictionary<string, lootData>>()
            {
                { "Ammunition", Ammunition }, { "Attire", Attire }, { "Component", Component }, { "Construction", Construction }, { "Food", Food }, { "Items", Items }, { "Medical", Medical },  { "Misc", Misc }, { "Tool", Tool }, { "Traps", Traps }, { "Resources", Resources }, { "Weapon", Weapon }, { "Electrical", Electrical }, { "Fun", Fun }
            };
        }

        public class CategorySettings
        {
            public int probability = 5;
            public bool allowBlueprints = true;
            public int blueprintChancePercent = 5;
        }

        public class lootData
        {
            public int probability = 5; 
            public int minStack = 1;
            public int maxStack = 1;
            public List<ulong> skins = new List<ulong>(); 
        }

        LootTable lootTable = new LootTable();
        public List<string> DefaultBlackList = new List<string> { "glue", "door.key", "note", "bleach", "ducttape", "blood", "tool.camera", "ammo.rocket.smoke", "battery", "bone.fragments", "blueprintbase", "water.salt", "jackhammer" };

        public void CreateLootTable(string lootTable)
        {
            lootTables[lootTable] = Interface.Oxide.DataFileSystem.ReadObject<LootTable>($"NPCLoot/{lootTable}");
            if (lootTables[lootTable].BlackList.Count == 0)
                lootTables[lootTable].BlackList = DefaultBlackList; 

            foreach (ItemDefinition rustItem in ItemManager.itemList)  
            {
                if ((IsJunk(lootTable, rustItem.shortname) || !IsReal(rustItem.shortname))
                || (!lootTables[lootTable].allowChristmas && rustItem.shortname.Contains("xmas")) || (!lootTables[lootTable].allowKeycards && rustItem.shortname.Contains("keycard_")))
                {
                    lootTables[lootTable].LootTypes[rustItem.category.ToString()].Remove(rustItem.shortname);
                    continue;
                }
                if (lootTables[lootTable].LootTypes[rustItem.category.ToString()].ContainsKey(rustItem.shortname)) continue;

                if (rustItem.category.ToString() == "Ammunition")
                    lootTables[lootTable].LootTypes["Ammunition"].Add(rustItem.shortname, new lootData() { minStack = 10, maxStack = 50 });
                else if (rustItem.category.ToString() == "Component")
                    lootTables[lootTable].LootTypes["Component"].Add(rustItem.shortname, new lootData() { minStack = 1, maxStack = 5 });
                else if (rustItem.category.ToString() == "Food")
                    lootTables[lootTable].LootTypes["Food"].Add(rustItem.shortname, new lootData() { minStack = 10, maxStack = 20 });
                else if (rustItem.category.ToString() == "Resources")
                    lootTables[lootTable].LootTypes["Resources"].Add(rustItem.shortname, new lootData() { minStack = 100, maxStack = 5000 });
                else if (rustItem.category.ToString() == "Traps")
                    lootTables[lootTable].LootTypes["Traps"].Add(rustItem.shortname, new lootData() { minStack = 1, maxStack = 5 });
                else lootTables[lootTable].LootTypes[rustItem.category.ToString()].Add(rustItem.shortname, new lootData() { minStack = 1, maxStack = 1 });
            }
            Interface.Oxide.DataFileSystem.WriteObject($"NPCLoot/{lootTable}", lootTables[lootTable]);
        } 

        public bool IsJunk(string lootTable, string rustItem) 
        {
            List<string> junk = lootTables[lootTable].BlackList;
            if (junk.Contains(rustItem))
                return true;
            return false;
        }

        public bool IsReal(string rustItem)
        {
            if (rustItem.Contains("bow.compound")) return false; 
            var createTest = ItemManager.CreateByName(rustItem, 1);
            if (createTest != null)
            {  
                createTest.Remove(0f);
                return true;
            } 
            return false;
        } 
        #endregion

        #region Config
        private ConfigData configData; 

        class ConfigData
        {
            public GlobalSettings GlobalSettings = new GlobalSettings();

            public static Settings Settings = new Settings();
            public Dictionary<string, Settings> CorpseTypes = new Dictionary<string, Settings>
            {
                {"MilitaryTunnel", Settings}, {"JunkPileScientists", Settings}, {"MountedScientist", Settings}, {"PeaceKeeperScientist", Settings}, {"BanditTown", Settings}, {"Scientist-Other", Settings}, {"Murderer", Settings}, {"BotSpawn", Settings}, 
            };
        }

        public class GlobalSettings
        {
            public bool corpseTypePerBotSpawnProfile = false; 
            public bool allowDuplicates = false; 
        }

        public class Settings
        {
            public string lootTable = "default"; 
            public bool enabled = false;
            public int maxItems = 6;
            public int minItems = 6;
            public bool gunsWithAmmo = false;
            public bool noGuns = false;
        }

        private void LoadConfigVariables()    
        {
            BotTypes.Clear();
            lootTableNames.Clear();
            configData = Config.ReadObject<ConfigData>();
            if (configData == null)
                LoadDefaultConfig();
            bool perBotSpawn = false; 
            List<string> removeList = new List<string>();
            if (configData.GlobalSettings.corpseTypePerBotSpawnProfile)
            {
                perBotSpawn = true;
                if (configData.CorpseTypes.ContainsKey("BotSpawn"))
                    configData.CorpseTypes.Remove("BotSpawn");
            }
              
            foreach (var entry in BotSpawnBots) 
            { 
                if (perBotSpawn && !configData.CorpseTypes.ContainsKey($"BotSpawn-{entry.Key}")) 
                    configData.CorpseTypes.Add($"BotSpawn-{entry.Key}", new Settings());
                else if (!perBotSpawn && configData.CorpseTypes.ContainsKey($"BotSpawn-{entry.Key}")) 
                    removeList.Add($"BotSpawn-{entry.Key}"); 
            } 

            foreach (var entry in configData.CorpseTypes)      
            {
                if ((!perBotSpawn && entry.Key.Contains("BotSpawn-")) 
                || (perBotSpawn && entry.Key.Contains("BotSpawn-") && !BotSpawnBots.ContainsKey(entry.Key.Remove(0,9)))) 
                    removeList.Add(entry.Key); 
                
                BotTypes.Add(entry.Key, new List<ulong>());
                if (!lootTableNames.Contains(entry.Value.lootTable))
                    lootTableNames.Add(entry.Value.lootTable);
            } 
            foreach (var entry in removeList) 
            {
                if (configData.CorpseTypes.ContainsKey(entry)) 
                    configData.CorpseTypes.Remove(entry);
            }
            SaveConfig(configData);
        }

        protected override void LoadDefaultConfig() 
        {
            Puts("Creating new config file.");
            configData = new ConfigData();
            //SaveConfig(new ConfigData());
        }

        void SaveConfig(ConfigData config) 
        {
            Config.WriteObject(config, true);
        } 
        #endregion    
    }
}