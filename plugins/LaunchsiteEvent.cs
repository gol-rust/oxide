using Oxide.Core.Plugins;
using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("LaunchsiteEvent", "The Noxes", "1.0")]
    [Description("Spawn airdrops at the top of launch site's towers at set intervals")]
    
    public class LaunchsiteEvent : RustPlugin
    {
        System.Random rnd = new System.Random();
        private const string triggerperm = "launchsiteevent.trigger";
        private List<Vector3> launchtowers = new List<Vector3> { new Vector3(243.8f, 311.8f, 247.2f), new Vector3(229.2f, 311.8f, 286.7f), new Vector3(267.7f, 311.8f, 301.0f), new Vector3(282.0f, 311.8f, 261.3f) };
        private List<Vector3> launchtowersnorth = new List<Vector3> { new Vector3(242.8f, 311.8f, 250.2f), new Vector3(228.2f, 311.8f, 289.7f), new Vector3(266.7f, 311.8f, 304.0f), new Vector3(281.0f, 311.8f, 264.3f) };
        // private List<Vector3> launchtowerssouth = new List<Vector3> { new Vector3(244.8f, 311.8f, 244.2f), new Vector3(230.2f, 311.8f, 283.7f), new Vector3(268.7f, 311.8f, 298.0f), new Vector3(283.0f, 311.8f, 258.3f) };
        private Dictionary<Vector3, BaseEntity> spawneddrop = new Dictionary<Vector3, BaseEntity>();
        private Dictionary<Vector3, BaseEntity> spawnedcrate = new Dictionary<Vector3, BaseEntity>();
        private Quaternion rotation = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
        private string supplydorp = "assets/prefabs/misc/supply drop/supply_drop.prefab";
        private string elitecrate = "assets/bundled/prefabs/radtown/crate_elite.prefab";

        private void OnServerInitialized()
        {
            timer.Repeat(3600, 0, CheckDrop);
        }

        void Init()
        {
            permission.RegisterPermission(triggerperm, this);
        }

        private void Unload()
        {
            var cratelist = spawnedcrate.Values.ToList();
            var droplist = spawneddrop.Values.ToList();
            var cleanuplist = cratelist.Concat(droplist).ToList();
            foreach (var item in cleanuplist) item.Kill(BaseNetworkable.DestroyMode.None);
        }

        private void CheckDrop()
        {
            Puts("CheckDrop triggered");
            Vector3 droplocation = PickRandomDropLocation();
            Vector3 cratelocation = PickRandomCrateLocation();
            if (spawneddrop.ContainsKey(droplocation))
            {
                spawneddrop[droplocation].Kill(BaseNetworkable.DestroyMode.None);
                spawneddrop.Remove(droplocation);
            }
            if (spawnedcrate.ContainsKey(cratelocation))
            {
                spawnedcrate[cratelocation].Kill(BaseNetworkable.DestroyMode.None);
                spawnedcrate.Remove(cratelocation);
            }
            SpawnDrop(droplocation, cratelocation);
            return;
        }

        private Vector3 PickRandomDropLocation()
        {
            int index = rnd.Next(launchtowers.Count);
            Vector3 randomposition = launchtowers[index];
            return randomposition;
        }

        private Vector3 PickRandomCrateLocation()
        {
            int index = rnd.Next(launchtowersnorth.Count);
            Vector3 randomposition = launchtowersnorth[index];
            return randomposition;
        }

        private void SpawnDrop(Vector3 droplocation, Vector3 cratelocation)
        {
            var newDrop = GameManager.server.CreateEntity(supplydorp, droplocation, rotation);
            var newCrate = GameManager.server.CreateEntity(elitecrate, cratelocation, rotation);
            Effect.server.Run("assets/prefabs/tools/c4/effects/c4_explosion.prefab", droplocation);
            newDrop.Spawn();
            newCrate.Spawn();
            spawneddrop.Add(droplocation, newDrop);
            spawnedcrate.Add(cratelocation, newCrate);
        }

        [ChatCommand("triggerdrop")]
        void trigger_command(BasePlayer player, string command, string[] args)
        {
            if (!permission.UserHasPermission(player.UserIDString, triggerperm))
            {
                SendReply(player, "You don't have access to that command.");
                return;
            }
            else
            {
                CheckDrop();
                return;
            }
        }

        [ConsoleCommand("triggerdrop")]
        void trigger_ccommand()
        {
            CheckDrop();
            return;
        }
    }
}