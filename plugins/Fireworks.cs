﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using Oxide.Core;
using Rust;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("Fireworks", "The Noxes", "1.2.1")]
    [Description("Make fireworks appear")]
    public class Fireworks : RustPlugin
    {
        System.Random rnd = new System.Random();
        const string UsePerm = "fireworks.use";
        List<string> ShortNameList = new List<string>{"ammo.rocket.basic", "ammo.rocket.hv", "ammo.rocket.smoke"};
        string HoboBarrel = "assets/bundled/prefabs/static/hobobarrel_static.prefab";
        string BarrelDecoration = "assets/prefabs/deployable/furnace.large/furnace.large.prefab";
        Dictionary<int, List<BaseEntity>> LaunchPadList = new Dictionary<int, List<BaseEntity>>();
        List<StorageContainer> BarrelList = new List<StorageContainer>();
        List<StorageContainer> CDList = new List<StorageContainer>();
        Timer RepeatTimer;

        #region
        class StoredData
        {
            public Dictionary<int, List<float>> Launchpads = new Dictionary<int, List<float>>();
            public int RepeatInMinutes = 0;
            public StoredData()
            {
            }
        }

        StoredData storedData;

        void SaveData() => Interface.Oxide.DataFileSystem.WriteObject("Fireworks", storedData);

        void OnNewSave(string filename)
        {
            storedData = new StoredData();
            SaveData();
        }
        #endregion

        #region
        Configuration config;
        public class Configuration
        {
            [JsonProperty(PropertyName = "How many fireworks are shot from launchpads and around players")]
            public int FWAmount { get; set; } = 15;
        }

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                config = Config.ReadObject<Configuration>();
                if (config == null)
                    LoadDefaultConfig();
            }
            catch
            {
                LoadDefaultConfig();
            }
            SaveConfig();
        }

        protected override void LoadDefaultConfig()
        {
            Puts("Generating new configuration file");
            config = new Configuration();
        }

        protected override void SaveConfig() => Config.WriteObject(config);
        #endregion

        void OnServerInitialized()
        {
            storedData = Interface.Oxide.DataFileSystem.ReadObject<StoredData>("Fireworks");
            permission.RegisterPermission(UsePerm, this);
            foreach (KeyValuePair<int, List<float>> entry in storedData.Launchpads)
            {
                Vector3 LaunchPosition = new Vector3(entry.Value[0], entry.Value[1], entry.Value[2]);
                SpawnLaunchpad(LaunchPosition, entry.Key);
            }
            if (storedData.RepeatInMinutes > 0)
                RepeatTimer = timer.Every(60 * storedData.RepeatInMinutes, () => {
                int amount = config.FWAmount;
                foreach (StorageContainer Barrel in BarrelList)
                    TriggerLaunchpad(Barrel);
                foreach (BasePlayer online in BasePlayer.activePlayerList)
                {
                    Vector3 OnlinePos = online.transform.position;
                    timer.Repeat(0.5f, amount, () => {
                        Vector3 LaunchPos = new Vector3(OnlinePos.x + rnd.Next(-40, 40), OnlinePos.y + 15, OnlinePos.z + rnd.Next(-40, 40));
                        RandomFirework(LaunchPos);
                    });
                }
            });
        }

        void Unload()
        {
            foreach (List<BaseEntity> EntList in LaunchPadList.Values)
            {
                foreach (BaseEntity ent in EntList)
                    ent.Kill();
            }
        }

        [ChatCommand("fireworks")]
        void FWCommand(BasePlayer player, string command, string[] args)
        {
            if (!permission.UserHasPermission(player.UserIDString, UsePerm))
                return;
            Vector3 PlayerPos = player.transform.position;
            if (args.Length == 0)
            {
                SendReply(player, "Either specify an amount of fireworks you wish to shoot up above you, or use the argument <color=#ffa500ff>players/all {amount (optional)}</color> to shoot fireworks above everyone online. Launchpads are fired too if <color=#ffa500ff>all</color> is used");
                return;
            }
            if (args[0] == "players")
            {
                foreach (BasePlayer online in BasePlayer.activePlayerList)
                {
                    int amount = config.FWAmount;
                    if (args.Length > 1)
                        amount = Convert.ToInt32(args[1]);
                    Vector3 OnlinePos = online.transform.position;
                    timer.Repeat(0.5f, amount, () => {
                        Vector3 LaunchPos = new Vector3(OnlinePos.x + rnd.Next(-40, 40), OnlinePos.y + 15, OnlinePos.z + rnd.Next(-40, 40));
                        RandomFirework(LaunchPos);
                    });
                    SendReply(online, "Enjoy the fireworks show!");
                }
            }
            if (args[0] == "all")
            {
                int amount = config.FWAmount;
                if (args.Length > 1)
                {
                    amount = Convert.ToInt32(args[1]);
                    foreach (StorageContainer Barrel in BarrelList)
                        TriggerLaunchpad(Barrel, amount);
                }
                else
                {
                    foreach (StorageContainer Barrel in BarrelList)
                        TriggerLaunchpad(Barrel);
                }
                foreach (BasePlayer online in BasePlayer.activePlayerList)
                {
                    Vector3 OnlinePos = online.transform.position;
                    timer.Repeat(0.5f, amount, () => {
                        Vector3 LaunchPos = new Vector3(OnlinePos.x + rnd.Next(-40, 40), OnlinePos.y + 15, OnlinePos.z + rnd.Next(-40, 40));
                        RandomFirework(LaunchPos);
                    });
                    SendReply(online, "Enjoy the fireworks show!");
                }
            }
            else
            {
                if (Convert.ToInt32(args[0]) <= 0)
                {
                    SendReply(player, "You do need to send at least 1 firework up.");
                    return;
                }
                timer.Repeat(0.5f, Convert.ToInt32(args[0]), () => RandomFirework(PlayerPos));
                SendReply(player, String.Format("Sent {0} firework(s) up!", args[0]));
            }
            Puts(String.Format("{0} triggered fireworks", player.displayName));
        }

        [ChatCommand("launchpad")]
        void LPCommand(BasePlayer player, string command, string[] args)
        {
            if (!permission.UserHasPermission(player.UserIDString, UsePerm))
                return;
            if (args.Length == 0)
            {
                SendReply(player, "You can either <color=#ffa500ff>add</color>, <color=#ffa500ff>remove</color> or <color=#ffa500ff>trigger</color> launchpads.");
                return;
            }
            List<string> argsList = new List<string>(args);
            switch (args[0].ToLower())
            {
                case "add":
                    {
                        int uid = GetId();
                        Vector3 LaunchPosition = FindSpot(player);
                        SpawnLaunchpad(LaunchPosition, uid);
                        storedData.Launchpads.Add(uid, new List<float> { LaunchPosition.x, LaunchPosition.y, LaunchPosition.z });
                        SaveData();
                        SendReply(player, String.Format("Successfully added launchpad #{0}", uid));
                        return;
                    }
                case "remove":
                    {
                        if (args.Length == 1)
                        {
                            SendConsoleReply(player.net.connection, "[Fireworks] Available IDs:");
                            foreach (KeyValuePair<int, List<float>> entry in storedData.Launchpads)
                                SendConsoleReply(player.net.connection, String.Format("{0}. at coordinates {1} {2} {3}", entry.Key, entry.Value[0], entry.Value[1], entry.Value[2]));
                            SendReply(player, "Check your console for available IDs, then run <color=#ffa500ff>/launchpad remove {IDs}</color> or <color=#ffa500ff>/launchpad remove all</color>");
                            return;
                        }
                        argsList.RemoveAt(0);
                        if (argsList[0] == "all")
                        {
                            Unload();
                            LaunchPadList = new Dictionary<int, List<BaseEntity>>();
                            BarrelList = new List<StorageContainer>();
                            storedData = new StoredData();
                            SaveData();
                            SendReply(player, "Successfully removed all launchpads!");
                            return;
                        }
                        foreach (string sid in argsList)
                        {
                            int uid = Convert.ToInt32(sid);
                            if (LaunchPadList.ContainsKey(uid))
                            {
                                foreach (BaseEntity ent in LaunchPadList[uid])
                                {
                                    StorageContainer storage = ent.GetComponent<StorageContainer>();
                                    if (BarrelList.Contains(storage))
                                        BarrelList.Remove(storage);
                                    ent.Kill();
                                }                                
                                LaunchPadList.Remove(uid);
                            }
                            if (storedData.Launchpads.ContainsKey(uid))
                                storedData.Launchpads.Remove(uid);
                            SaveData();
                            SendReply(player, String.Format("Removed launchpad #{0}!", uid));
                        }
                        return;
                    }
                case "trigger":
                    {
                        if (args.Length == 1)
                        {
                            SendConsoleReply(player.net.connection, "[Fireworks] Available IDs:");
                            foreach (KeyValuePair<int, List<float>> entry in storedData.Launchpads)
                                SendConsoleReply(player.net.connection, String.Format("{0}. at coordinates {1} {2} {3}", entry.Key, entry.Value[0], entry.Value[1], entry.Value[2]));
                            SendReply(player, "Check your console for available IDs, then run <color=#ffa500ff>/launchpad trigger all {amount (optional)}</color> or <color=#ffa500ff>/launchpad trigger {IDs}</color>");
                            return;
                        }
                        argsList.RemoveAt(0);
                        if (argsList[0] == "all")
                        {
                            if (argsList.Count() > 1)
                            {
                                foreach (StorageContainer Barrel in BarrelList)
                                    TriggerLaunchpad(Barrel, Convert.ToInt32(argsList[1]));
                            }
                            else
                            {
                                foreach (StorageContainer Barrel in BarrelList)
                                    TriggerLaunchpad(Barrel);
                            }
                            SendReply(player, "All launchpads are now shooting fireworks!");
                            foreach (BasePlayer online in BasePlayer.activePlayerList)
                                SendReply(online, "Enjoy the fireworks show!");
                            Puts(String.Format("{0} triggered fireworks", player.displayName));
                            return;
                        }
                        foreach (string sid in argsList)
                        {
                            int uid = Convert.ToInt32(sid);
                            if (LaunchPadList.ContainsKey(uid))
                            {
                                BaseEntity Barrel = LaunchPadList[uid][0];
                                StorageContainer storage = Barrel.GetComponent<StorageContainer>();
                                TriggerLaunchpad(storage);
                            }
                        }
                        SendReply(player, "All requested launchpads are now shooting fireworks!");
                        Puts(String.Format("{0} triggered fireworks", player.displayName));
                        return;
                    }
                case "refill":
                    {
                        foreach (StorageContainer Barrel in BarrelList)
                        {
                            Item item = ItemManager.CreateByPartialName("wood", 10000);
                            item.MoveToContainer(Barrel.inventory);
                        }
                        SendReply(player, "All launchpads were restocked with rockets!");
                        return;
                    }
                default:
                    {
                        SendReply(player, "You can either <color=#ffa500ff>add</color>, <color=#ffa500ff>remove</color> or <color=#ffa500ff>trigger</color> launchpads.");
                        return;
                    }
            }
        }

        [ConsoleCommand("fireworks")]
        void cfwCommand(ConsoleSystem.Arg arg)
        {
            if (arg.Connection == null)
            {
                if (arg.Args == null)
                {
                    string timerRepeat = "not launching every x minutes.\nUse \'fireworks timer x\' to start launching.";
                    if (storedData.RepeatInMinutes > 0)
                        timerRepeat = String.Format("launching every {0} minutes.\nUse \'fireworks stop\' to stop launching.", storedData.RepeatInMinutes);
                    Puts(String.Format("Fireworks are currently {0}\nUse \'fireworks launch\' to launch fireworks from launchpads and above players.", timerRepeat));
                    return;
                }
                if (arg.Args[0].ToLower() == "timer")
                {
                    if (arg.Args.Length < 2)
                    {
                        Puts("You need to specify how often, in minutes, to set off all fireworks. Use \'fireworks timer x\' to do this.");
                        return;
                    }
                    if (storedData.RepeatInMinutes > 0 && RepeatTimer != null)
                    {
                        Puts(String.Format("Fireworks are already being launched every {0} minutes. Run \'fireworks stop\' before setting a new timer.", storedData.RepeatInMinutes));
                        return;
                    }
                    storedData.RepeatInMinutes = Convert.ToInt32(arg.Args[1]);
                    SaveData();
                    RepeatTimer = timer.Every(60 * storedData.RepeatInMinutes, () =>
                    {
                        int amount = config.FWAmount;
                        foreach (StorageContainer Barrel in BarrelList)
                            TriggerLaunchpad(Barrel);
                        foreach (BasePlayer online in BasePlayer.activePlayerList)
                        {
                            Vector3 OnlinePos = online.transform.position;
                            timer.Repeat(0.5f, amount, () => {
                                Vector3 LaunchPos = new Vector3(OnlinePos.x + rnd.Next(-40, 40), OnlinePos.y + 15, OnlinePos.z + rnd.Next(-40, 40));
                                RandomFirework(LaunchPos);
                            });
                        }
                    });
                    Puts(String.Format("Started launching fireworks every {0} minutes", storedData.RepeatInMinutes));
                }
                if (arg.Args[0].ToLower() == "stop")
                {
                    if (RepeatTimer == null)
                    {
                        Puts("There are no fireworks to stop from launching.");
                        if (storedData.RepeatInMinutes > 0)
                        {
                            storedData.RepeatInMinutes = 0;
                            SaveData();
                        }
                        return;
                    }
                    RepeatTimer.Destroy();
                    RepeatTimer = null;
                    storedData.RepeatInMinutes = 0;
                    SaveData();
                }
                if (arg.Args[0].ToLower() == "launch")
                {
                    int amount = config.FWAmount;
                    if (arg.Args.Length > 1)
                    {
                        amount = Convert.ToInt32(arg.Args[1]);
                        foreach (StorageContainer Barrel in BarrelList)
                            TriggerLaunchpad(Barrel, amount);
                    }
                    else
                    {
                        foreach (StorageContainer Barrel in BarrelList)
                            TriggerLaunchpad(Barrel);
                    }
                    foreach (BasePlayer online in BasePlayer.activePlayerList)
                    {
                        Vector3 OnlinePos = online.transform.position;
                        timer.Repeat(0.5f, amount, () => {
                            Vector3 LaunchPos = new Vector3(OnlinePos.x + rnd.Next(-40, 40), OnlinePos.y + 15, OnlinePos.z + rnd.Next(-40, 40));
                            RandomFirework(LaunchPos);
                        });
                        SendReply(online, "Enjoy the fireworks show!");
                    }
                    Puts("Server Console triggered fireworks");
                }
            }
        }

        void SendConsoleReply(Network.Connection cn, string msg)
        {
            if (Network.Net.sv.IsConnected())
            {
                Network.Net.sv.write.Start();
                Network.Net.sv.write.PacketID(Network.Message.Type.ConsoleMessage);
                Network.Net.sv.write.String(msg);
                Network.Net.sv.write.Send(new Network.SendInfo(cn));
            }
        }

        int GetId()
        {
            int LastID = 0;
            if (storedData.Launchpads.Count() > 0)
                LastID = storedData.Launchpads.Keys.Last();
            int uid = LastID + 1;
            return uid;
        }

        void ScaleAllDamage(List<DamageTypeEntry> damageTypes, float scale)
        {
            for (int i = 0; i < damageTypes.Count; i++)
            {
                damageTypes[i].amount *= scale;
            }
        }

        void RandomFirework(Vector3 origin)
        {
            float minLaunchAngle = -3f;
            float maxLaunchAngle = 0f;
            float launchHeight = 10f;
            Vector3 RandomDirection = new Vector3(UnityEngine.Random.Range(-1f, 1f), 0, UnityEngine.Random.Range(-1f, 1f));
            var launchAngle = UnityEngine.Random.Range(minLaunchAngle, maxLaunchAngle);
            var launchDirection = (Vector3.up * -launchAngle + RandomDirection).normalized;
            Vector3 launchPosition = new Vector3(origin.x, origin.y + launchHeight, origin.z);

            // Choose what firework to use
            ItemDefinition projectileItem;
            string ChosenFirework = ShortNameList[rnd.Next(ShortNameList.Count)];
            projectileItem = ItemManager.FindItemDefinition(ChosenFirework);

            // Create the in-game "fireworks" entity:
            var component = projectileItem.GetComponent<ItemModProjectile>();
            var entity = GameManager.server.CreateEntity(component.projectileObject.resourcePath, launchPosition, new Quaternion(), true);

            // Set fireworks speed and detonation time:
            var serverProjectile = entity.GetComponent<ServerProjectile>();
            TimedExplosive timedExplosive = entity.GetComponent<TimedExplosive>();
            serverProjectile.gravityModifier = -0.02f;
            serverProjectile.speed = 100;
            timedExplosive.timerAmountMin = 15f;
            timedExplosive.timerAmountMax = 25f;
            entity.SendMessage("InitializeVelocity", (object)(launchDirection * 1.0f));
            ScaleAllDamage(timedExplosive.damageTypes, 0);
            entity.Spawn();
        }

        Vector3 FindSpot(BasePlayer player)
        {
            var ray = new Ray(player.eyes.position, player.eyes.HeadForward());
            RaycastHit hit;
            Physics.Raycast(ray, out hit, 5000);
            Vector3 HitLocation = hit.point;
            return HitLocation;
        }

        void SpawnLaunchpad(Vector3 LaunchPosition, int uid)
        {
            BaseEntity Barrel = GameManager.server.CreateEntity(HoboBarrel, LaunchPosition);
            BaseEntity UnderBarrel = GameManager.server.CreateEntity(BarrelDecoration, new Vector3(LaunchPosition.x, LaunchPosition.y - 5, LaunchPosition.z));
            Barrel.Spawn();
            StorageContainer Storage = Barrel.GetComponent<StorageContainer>();
            Item item = ItemManager.CreateByPartialName("wood", 10000);
            item.MoveToContainer(Storage.inventory);
            UnderBarrel.Spawn();
            BarrelList.Add(Storage);
            LaunchPadList[uid] = new List<BaseEntity> { Barrel, UnderBarrel };
        }

        void TriggerLaunchpad(StorageContainer container, int amount = 0)
        {
            if (amount == 0)
                amount = config.FWAmount;
            BaseOven Barrel = container as BaseOven;
            Vector3 BarrelPos = container.transform.position;
            Vector3 LaunchPos = new Vector3(BarrelPos.x, BarrelPos.y - 8, BarrelPos.z);
            Barrel.allowByproductCreation = false;
            Barrel.StartCooking();
            CDList.Add(container);
            timer.Repeat(0.5f, amount, () => RandomFirework(LaunchPos));
            timer.Once(amount / 2, () => {
                Barrel.StopCooking();
                CDList.Remove(container);
            });
        }

        object CanLootEntity(BasePlayer player, StorageContainer container)
        {
            if (CDList.Contains(container))
            {
                SendReply(player, "This launcher is still shooting fireworks!");
                return false;
            }
            if (BarrelList.Contains(container))
            {
                SendReply(player, "Fireworks will now be shot, stand back and enjoy the show!");
                TriggerLaunchpad(container);
                return false;
            }
            return null;
        }

        object OnEntityTakeDamage(BaseCombatEntity entity, HitInfo info)
        {
            foreach (List<BaseEntity> LaunchComponents in LaunchPadList.Values)
            {
                if (LaunchComponents.Contains(entity))
                    return false;
            }
            return null;
        }

    }
}