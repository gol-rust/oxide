﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Oxide.Plugins
{
    [Info("Default Item Skin", "The Noxes", "0.0.3")]
    [Description("Sets default skin of defined items")]
    class DefaultItemSkin : RustPlugin
    {
        Configuration config;
        public class Configuration
        {
            [JsonProperty(PropertyName = "List of items (use the shortname) and their desired default skins")]
            public Dictionary<string, ulong> DefaultDict { get; set; } = new Dictionary<string, ulong>();       
        }

        protected override void LoadConfig()
        {
            base.LoadConfig();
            try
            {
                config = Config.ReadObject<Configuration>();
                if (config == null)
                    LoadDefaultConfig();
            }
            catch
            {
                LoadDefaultConfig();
            }
            SaveConfig();
        }

        protected override void LoadDefaultConfig()
        {
            Puts("No or faulty config detected. Generating new configuration file");
            config = new Configuration();
        }

        protected override void SaveConfig() => Config.WriteObject(config);

        void OnItemCraftFinished(ItemCraftTask task, Item item)
        {
            if (config.DefaultDict.ContainsKey(item.info.shortname) && item.skin == 0)
                item.skin = config.DefaultDict[item.info.shortname];
        }
    }
}