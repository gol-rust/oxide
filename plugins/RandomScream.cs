﻿using System;
using UnityEngine;

namespace Oxide.Plugins
{
    [Info("RandomScream", "The Noxes", "1.0.0")]
    [Description("Scream at random")]

    public class RandomScream : RustPlugin
    {
        const string isClumsy = "randomscream.clumsy";
        const string effectName = "assets/bundled/prefabs/fx/player/beartrap_scream.prefab";

        void Init()
        {
            permission.RegisterPermission(isClumsy, this);
        }

        void OnDoorOpened(Door door, BasePlayer player)
        {
            int screamChance = 1;
            if (permission.UserHasPermission(player.UserIDString, isClumsy))
                screamChance = 5000;
            System.Random rnd = new System.Random();
            int chance = rnd.Next(10000);
            if (chance <= screamChance)
            {
                Effect.server.Run(effectName, player.transform.position);
                SendReply(player, "You stubbed your toe on the doorframe.");
                return;
            }
        }
                
    }
}
