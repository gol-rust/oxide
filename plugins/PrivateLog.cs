﻿using Oxide.Core.Libraries.Covalence;
using System.Linq;

namespace Oxide.Plugins
{
    [Info("PrivateLog", "The Noxes", "1.0.4")]
    [Description("Log specific things separately")]
    public class PrivateLog : CovalencePlugin
    {
#if RUST
        void OnServerCommand(ConsoleSystem.Arg arg)
        {
            if (arg.Connection == null) return;

            var command = arg.cmd.FullName;
            var args = arg.GetString(0);
            string[] argsList = args.Split(' ');
            string currentTime = System.DateTime.Now.ToShortTimeString();
            string playerName = arg.Connection.username;
            ulong playerId = arg.Connection.userid;
            if (command.Contains("relationshipmanager"))
            {
                if (command.Contains("trycreateteam"))
                    LogToFile("teams", string.Format("[{0}] {1} ({2}) created a team", currentTime, playerName, playerId), this);
                if (command.Contains("sendinvite"))
                    LogToFile("teams", string.Format("[{0}] {1} ({2}) invited {3} ({4})", currentTime, playerName, playerId, players.FindPlayerById(args).Name, args), this);
                if (command.Contains("acceptinvite"))
                    LogToFile("teams", string.Format("[{0}] {1} ({2}) accepted an invite", currentTime, playerName, playerId), this);
                if (command.Contains("rejectinvite"))
                    LogToFile("teams", string.Format("[{0}] {1} ({2}) declined an invite", currentTime, playerName, playerId), this);
                if (command.Contains("leaveteam"))
                    LogToFile("teams", string.Format("[{0}] {1} ({2}) left their team", currentTime, playerName, playerId), this);
                if (command.Contains("promote"))
                    LogToFile("teams", string.Format("[{0}] {1} ({2}) promoted someone else to leader", currentTime, playerName, playerId), this);
                if (command.Contains("kickmember"))
                    LogToFile("teams", string.Format("[{0}] {1} ({2}) kicked {3} ({4})", currentTime, playerName, playerId, players.FindPlayerById(args).Name, args), this);
            }
            if (command.Contains("note.update"))
        LogToFile("notes", string.Format("[{0}] {1} ({2}) wrote the following on note {3}:\n{4}", currentTime, playerName, playerId, args, arg.GetString(1)), this);
        }
#endif
    }
}