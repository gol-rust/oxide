﻿using Oxide.Core.Plugins;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Oxide.Plugins
{
    [Info("CommandProxy", "The Noxes", "1.5")]
    [Description("Create proxy commands for commands that needs a proxy command.")]
    

    public class CommandProxy : RustPlugin
    {

        private const string kickperm = "commandproxy.kick";
        private const string banperm = "commandproxy.ban";
        private const string sayperm = "commandproxy.say";
        private const string helptext = "Usage: /admin kick|ban|say name|text (reason)";
        private const string accesstext = "Access insufficient.";

        void Init()
        {
            permission.RegisterPermission(kickperm, this);
            permission.RegisterPermission(banperm, this);
            permission.RegisterPermission(sayperm, this);
        }

        
        [ChatCommand("admin")]
        void admin_command(BasePlayer player, string command, string[] args)
        {
            if (permission.UserHasPermission(player.UserIDString, kickperm) || permission.UserHasPermission(player.UserIDString, banperm) || permission.UserHasPermission(player.UserIDString, sayperm))
            {
                if (args.Length == 0)
                {
                    SendReply(player, helptext);
                    return;
                }
                switch (args[0].ToLower())
                {
                    case "kick":
                    {
                        if (!permission.UserHasPermission(player.UserIDString, kickperm))
                        {
                            SendReply(player, accesstext);
                            return;
                        }
                        if (args.Length == 1 || args.Length > 3)
                        {
                            SendReply(player, helptext);
                            return;
                        }
                        if (args.Length == 2)
                        {
                            rust.RunServerCommand(string.Format("kick \"{0}\"", args[1]));
                            return;
                        }
                        if (args.Length == 3)
                        {
                            rust.RunServerCommand(string.Format("kick \"{0}\" \"{1}\"", args[1], args[2]));
                            return;
                        }
                        break;
                    }
                    case "ban":
                    {
                        if (!permission.UserHasPermission(player.UserIDString, banperm))
                        {
                            SendReply(player, accesstext);
                            return;
                        }
                        if (args.Length == 1 || args.Length > 3)
                        {
                            SendReply(player, helptext);
                            return;
                        }
                        if (args.Length == 2)
                        {
                            rust.RunServerCommand(string.Format("ban \"{0}\"", args[1]));
                            return;
                        }
                        if (args.Length == 3)
                        {
                            rust.RunServerCommand(string.Format("ban \"{0}\" \"{1}\"", args[1], args[2]));
                            return;
                        }
                        break;
                    }
                    case "say":
                    {
                        if (!permission.UserHasPermission(player.UserIDString, sayperm))
                        {
                            SendReply(player, accesstext);
                            return;
                        }
                        if (args.Length == 1 || args.Length > 2)
                        {
                            SendReply(player, helptext);
                            return;
                        }
                        else 
                        {
                            rust.RunServerCommand(string.Format("say {0}", args[1]));
                            return;
                        }
                        break;                        
                    }
                    default:
                    {
                        SendReply(player, helptext);
                        return;
                    }
                }
            }
            else 
            {
                SendReply(player, accesstext);
                return;
            }
        }
        [ChatCommand("wipeme")]
        void wipebp_command(BasePlayer player, string command, string[] args)
        {
            var blueprintComponent = player.blueprints;
            if (blueprintComponent == null)
                return;

            blueprintComponent.Reset();
            SendReply(player, "Your blueprints were successfully wiped!");
            return;
        }
    }
}
