using System.Collections.Generic;
using Oxide.Core.Libraries.Covalence;

namespace Oxide.Plugins
{
    [Info("ChatColor", "The Noxes", "1.0.5")]
    [Description("Allows trusted users to change their chat color")]
    public class ChatColor : CovalencePlugin
    {
        List<string> availableColors = new List<string> { "default", "black", "darkgreen", "orange", "pink", "purple", "silver", "skyblue", "yellow" };
        const string permAllow = "chatcolor.allow";

        void Init()
        {
            permission.RegisterPermission(permAllow, this);
        }

        [Command("namecolor")]
        void ColorCommand(IPlayer player, string command, string[] args)
        {
            if (!player.HasPermission(permAllow))
            {
                player.Reply("You don't have access to this command");
                return;
            }
            string helptext = string.Format("The following colors are available:\n{0}\nUse <color=#ffa500ff>/namecolor color</color> to switch", string.Join("\n", availableColors.ToArray()));
            if (args.Length == 0)
            {
                player.Reply(helptext);
                return;
            }
            if (args[0] == "help")
            {
                player.Reply(helptext);
                return;
            }
            if (!availableColors.Contains(args[0]))
            {
                player.Reply(string.Format("This color is not available. Please choose one of the following:\n{0}", string.Join("\n", availableColors.ToArray())));
                return;
            }            
            foreach(string color in availableColors)
            {
                string actualColor = string.Format("trusted{0}", color);
                server.Command(string.Format("oxide.usergroup remove {0} {1}", player.Id, actualColor));
            }
            if (args[0] != "default")
            {
                string wantedColor = string.Format("trusted{0}", args[0]);
                server.Command(string.Format("oxide.usergroup add {0} {1}", player.Id, wantedColor));
            }
            player.Reply(string.Format("Your name is now {0}!", args[0]));
            return;
        }
    }
}